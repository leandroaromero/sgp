<?php

/**
 * Sitio form.
 *
 * @package    sgp
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class SitioAdminForm extends BaseSitioForm
{
  protected $scheduledForDeletion = array();
  protected $scheduledForDeletionD = array();
  protected $scheduledForDeletionI = array();
  
  public function configure()
  {
    unset($this['created_at'],$this['created_by'], $this['updated_at'], $this['update_by']);
    unset($this['orden_identificacion']);
    $this->getObject()->setOrdenIdentificacion(1);
     

    $this->widgetSchema['localidad_id']->setOption('table_method', 'getOrderNombre');   
    $this->widgetSchema['departamento_id'] = new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Departamento'), 'add_empty' => true));
      
    $this->widgetSchema['tipo_de_yacimiento_id']->setOption('table_method', 'getOrderNombre'); 
    $this->widgetSchema['departamento_id']->setOption('table_method', 'getOrderNombre');
    $this->widgetSchema['descripcion_ubicacion']= new sfWidgetFormTextareaTinyMCE(array(
        'width'   => 400,
        'height'  => 200,
        'config'  => 'theme_advanced_buttons1 : "cut, copy, paste, images, bold, italic, underline, justifyleft, justifycenter, justifyright , outdent, indent, bullist, numlist, undo, redo, link",
                       theme_advanced_buttons2 : "",            
                       theme_advanced_buttons3 : "",
                       theme_advanced_toolbar_location : "top",
                       theme_advanced_toolbar_align : "left",
                       theme_advanced_statusbar_location : "bottom",
                       theme_advanced_resizing: "false",
                       plugins: "images, paste",
                       '),array('class' => 'tinyMCE',)
      );  
      
    $this->widgetSchema['observacion_tipo_yacimiento']= new sfWidgetFormTextareaTinyMCE(array(
        'width'   => 400,
        'height'  => 200,
        'config'  => 'theme_advanced_buttons1 : "cut, copy, paste, images, bold, italic, underline, justifyleft, justifycenter, justifyright , outdent, indent, bullist, numlist, undo, redo, link",
                       theme_advanced_buttons2 : "",            
                       theme_advanced_buttons3 : "",
                       theme_advanced_toolbar_location : "top",
                       theme_advanced_toolbar_align : "left",
                       theme_advanced_statusbar_location : "bottom",
                       theme_advanced_resizing: "false",
                       plugins: "images, paste",
                       '),array('class' => 'tinyMCE',)
      );   
      
    $this->widgetSchema['observaciones']= new sfWidgetFormTextareaTinyMCE(array(
        'width'   => 500,
        'height'  => 300,
        'config'  => 'theme_advanced_buttons1 : "cut, copy, paste, images, bold, italic, underline, justifyleft, justifycenter, justifyright , outdent, indent, bullist, numlist, undo, redo, link",
                       theme_advanced_buttons2 : "",            
                       theme_advanced_buttons3 : "",
                       theme_advanced_toolbar_location : "top",
                       theme_advanced_toolbar_align : "left",
                       theme_advanced_statusbar_location : "bottom",
                       theme_advanced_resizing: "false",
                       plugins: "images, paste",
                       '),array('class' => 'tinyMCE',)
      );       
      
      
      
      
    $this->widgetSchema['categoria_list']->setOption('renderer_class', 'sfWidgetFormSelectDoubleList');
     // $this->widgetSchema['trayectoria_list']->setOption('table_method', 'getOrderNombre');
    $this->widgetSchema['categoria_list']->setOption('label', 'Categorías');
      
    $this->widgetSchema['documentacion_inedita_list']->setOption('renderer_class', 'sfWidgetFormSelectDoubleList');
      //$this->widgetSchema['trayectoria_list']->setOption('table_method', 'getOrderNombre');
    $this->widgetSchema['documentacion_inedita_list']->setOption('label', 'Documentaciones Inéditas');
      
    $this->widgetSchema['fuente_bibliografica_list']->setOption('renderer_class', 'sfWidgetFormSelectDoubleList');
      //$this->widgetSchema['trayectoria_list']->setOption('table_method', 'getOrderNombre');
    $this->widgetSchema['fuente_bibliografica_list']->setOption('label', 'Bibliografía');
      
    $this->widgetSchema['reglamentacion_list']->setOption('renderer_class', 'sfWidgetFormSelectDoubleList');
      //$this->widgetSchema['trayectoria_list']->setOption('table_method', 'getOrderNombre');
    $this->widgetSchema['reglamentacion_list']->setOption('label', 'Reglamentación');
    


      $array1= array();
      $array1= $this->getObject()->getDescripcionGeneral();

      $renglones_forms = new SfForm();
      
      $count = 0;
        foreach ($array1 as $renglon) {
          $renglon_form = new DescripcionGeneralForm($renglon);
          $renglones_forms->embedForm($count, $renglon_form);
          $count ++;
        }
          //Empotramos el contenedor en el formulario principal
            $this->embedForm('descripcion_general', $renglones_forms);      
            
     
      $array2= array();
      $array2= $this->getObject()->getIntervencion();
      
      $renglones_forms = new SfForm();
      
      $count = 0;
        foreach ($array2 as $renglon) {
          $renglon_form = new IntervencionSitioForm($renglon);
          $renglones_forms->embedForm($count, $renglon_form);
          $count ++;
        }
          //Empotramos el contenedor en el formulario principal
      $this->embedForm('intervenciones', $renglones_forms);          
  
            
            
      $array3= array();
      $array3= $this->getObject()->getOtroNombre();

      $renglones_forms = new SfForm();
      
      $count = 0;
            foreach ($array3 as $renglon) {
                    $renglon_form = new OtroNombreForm($renglon);
                    $renglones_forms->embedForm($count, $renglon_form);
                    $count ++;
            }
          //Empotramos el contenedor en el formulario principal
            $this->embedForm('otros_nombres', $renglones_forms);           
  }
  

  
    public function addDescriptionGeneral($num){
        
          $general = new DescripcionGeneral();
          $general->setSitio($this->getObject());
          $general_form = new DescripcionGeneralForm($general);

          unset($general_form['sitio_id']);
          //Empotramos la nueva pícture en el contenedor
          $this->embeddedForms['descripcion_general']->embedForm($num, $general_form);
          //Volvemos a empotrar el contenedor
          $this->embedForm('descripcion_general', $this->embeddedForms['descripcion_general']);
    }

    public function addOtrosNombres($num){

          $name = new OtroNombre();
          $name->setSitio($this->getObject());
          $name_form = new OtroNombreForm($name);

          unset($name_form['sitio_id']);
          //Empotramos la nueva pícture en el contenedor
          $this->embeddedForms['otros_nombres']->embedForm($num, $name_form);
          //Volvemos a empotrar el contenedor
          $this->embedForm('otros_nombres', $this->embeddedForms['otros_nombres']);

      }
      public function addIntervencion($num){
        
          $intervencion = new Intervencion();
          $intervencion->setSitio($this->getObject());
          $intervencion_form = new IntervencionSitioForm($intervencion);

          unset($intervencion_form['sitio_id']);
          //Empotramos la nueva pícture en el contenedor
          $this->embeddedForms['intervenciones']->embedForm($num, $intervencion_form);
          //Volvemos a empotrar el contenedor
          $this->embedForm('intervenciones', $this->embeddedForms['intervenciones']);
    }
    

    public function bind(array $taintedValues = null, array $taintedFiles = null)
        {
          
          foreach($taintedValues['otros_nombres'] as $key=>$nameValues)
          {
            if (!isset($this['otros_nombres'][$key]) )
            {
             if ('' === trim($nameValues['otro_nombre']) )
            {
              unset($taintedValues['otro_nombre'][$key]);
             } else{
                $this->addOtrosNombres($key);
               }
            }
          }
          foreach($taintedValues['descripcion_general'] as $key=>$descriptionValues)
          {
            if (!isset($this['descripcion_general'][$key]) )
            {
             if ('' === trim($descriptionValues['texto_descripcion']))
              {
                 unset($taintedValues['descripcion_general'][$key]);
              } else{
                $this->addDescriptionGeneral($key);
              }
            }
          }     
         

          if( isset($taintedValues['intervenciones']) )
          {    
            foreach($taintedValues['intervenciones'] as $key=>$interValues)
            {
              if (!isset($this['intervenciones'][$key]) )
              {
                 if ('' === trim($interValues['descripcion_intervencion']))
                  {
                     unset($taintedValues['intervenciones'][$key]);
                  } else{
                      $this->addIntervencion($key);
                  }
              }
            }
          }else{
            $taintedValues['intervenciones']= array();
          }


            parent::bind($taintedValues, $taintedFiles);
        }


 protected function doBind(array $values)
  {
     
      foreach ($values['otros_nombres'] as $index => $nameValues)
      {
         if ('' === trim($nameValues['otro_nombre']) )
            {
              unset($values['otros_nombres'][$index]);
            }
        if ( isset($nameValues['delete']) && $nameValues['id'])
        {
          $this->scheduledForDeletion[$index] = $nameValues['id'];
          
        }
      }
      foreach ($values['descripcion_general'] as $i => $desValues)
      {
         if ('' === trim($desValues['texto_descripcion']))
            {
              unset($values['descripcion_general'][$i]);
            }
        if (isset($desValues['delete']))
        {
          $this->scheduledForDeletionD[$i] = $desValues['id'];
        }
      }
      
      foreach ($values['intervenciones'] as $ii => $interValues)
      {
        if ('' === trim($interValues['descripcion_intervencion']))
            {
              unset($values['intervenciones'][$ii]);
            }
        if (isset($interValues['delete']))
        {
          $this->scheduledForDeletionI[$ii] = $interValues['id'];       
        }
      }

    parent::doBind($values);

  }



  protected function doUpdateObject($values)
  {
  
    if (count($this->scheduledForDeletion))
    {
      foreach ($this->scheduledForDeletion as $index => $id)
      {
        
        Doctrine::getTable('OtroNombre')->findOneById($id)->delete();
        unset($values['otros_nombres'][$index]);
        unset( $this->getObject['otros_nombres'][$index]);
      }
     
    }
     foreach ($this->scheduledForDeletionD as $index => $id1)
      {        
        Doctrine::getTable('DescripcionGeneral')->findOneById($id1)->delete();
        unset($values['descripcion_general'][$index]);
        unset( $this->getObject['descripcion_general'][$index]);

      }
     foreach ($this->scheduledForDeletionI as $index => $id2)
      {        
        Doctrine::getTable('Intervencion')->findOneById($id2)->delete();


      }

    $this->getObject()->fromArray($values);
         
  }
 
}
