<?php

require_once dirname(__FILE__).'/../lib/imagenGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/imagenGeneratorHelper.class.php';

/**
 * imagen actions.
 *
 * @package    sgp
 * @subpackage imagen
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class imagenActions extends autoImagenActions
{
    public function executeNew(sfWebRequest $request)
  {
    $this->sitio_id=$request->getParameter('sitio_id');
    $this->imagen = new Imagen();
    $this->imagen->setSitioId($this->sitio_id);
    $this->form = new ImagenAdminForm($this->imagen);
  }


  public function executeDelete(sfWebRequest $request)
  {
    $request->checkCSRFProtection();
    $dir= $this->getUser()->getFile();
    $this->forward404Unless($picture = Doctrine::getTable('Imagen')->find(array($request->getParameter('id'))), sprintf('Object picture does not exist (%s).', $request->getParameter('id')));
    $OId=$picture->getSitioId();
    $filename = $picture->getArchivo();
    $filepath = sfConfig::get('sf_upload_dir').'/'.$dir.'/fotos/'.$filename;
    @unlink($filepath);
    $filepath1 = sfConfig::get('sf_upload_dir').'/'.$dir.'/fotos/thumbs/'.$filename;
    unlink($filepath1);
    $filepath2 = sfConfig::get('sf_upload_dir').'/'.$dir.'/fotos/big/'.$filename;
    @unlink($filepath2);
    $picture->delete();

     $this->redirect('sitio/edit?id='.$OId);
  }
  public function executeList_cancel($request){
    //$this->forward404Unless($picture = Doctrine::getTable('Imagen')->find(array($request->getParameter('id'))), sprintf('Object picture does not exist (%s).', $request->getParameter('id')));

    $OId=$request->getParameter('o_id');
    $this->redirect('sitio/edit?id='.$OId);
  }
  public function executeUpdate(sfWebRequest $request)
  {

    $this->forward404Unless($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT));
    $this->forward404Unless($picture = Doctrine::getTable('Imagen')->find(array($request->getParameter('id'))), sprintf('Object picture does not exist (%s).', $request->getParameter('id')));
    $a= array();
    $a=$request->getParameter('imagen');
	if(isset( $a['archivo']) ){
		$filename = $picture->getArchivo();
		$dir= $this->getUser()->getFile();
		$filepath = sfConfig::get('sf_upload_dir').'/'.$dir.'/fotos/'.$filename;
		@unlink($filepath);
		$filepath1 = sfConfig::get('sf_upload_dir').'/'.$dir.'/fotos/thumbs/'.$filename;
		@unlink($filepath1);
		$filepath2 = sfConfig::get('sf_upload_dir').'/'.$dir.'/fotos/big/'.$filename;
		@unlink($filepath2);
	}
    $this->form = new ImagenAdminForm($picture);
    $this->processForm($request, $this->form);
    $this->redirect('imagen/edit?id='.$picture->getId());
  }

  public function executeDownload(sfwebRequest $request)
  {
    $picture = Doctrine_Core::getTable('Imagen')->find($request->getParameter('i_id'));
    $this->forward404Unless($picture);
    $dir= $this->getUser()->getFile(); 
    $filename = $picture->getArchivo();
    $path = sfConfig::get('sf_upload_dir').'/'.$dir.'/fotos/big/'.$filename;

    $name= $picture->getSitio()->getIdentificacion(); 
    if($picture->getTitulo()){
		$name= $name.' '.$picture->getTitulo(); 
		}  
    
    $type = '';

    if (is_file($path)) {
        $size = filesize($path);
        if (function_exists('mime_content_type')) {
            $type = mime_content_type($path);
         } else if (function_exists('finfo_file')) {
                   $info = finfo_open(FILEINFO_MIME);
                   $type = finfo_file($info, $path);
                   finfo_close($info);
                }
         if ($type == '') {
             $type = "application/force-download";
            }
    $file = basename($name); 
    header("Content-Type: $type");
    header("Content-Disposition: attachment; filename=\"$file\"");
    header("Content-Transfer-Encoding: binary");
    header("Content-Length: " . $size);
    // descargar achivo
    readfile($path);
    } else {
         die("File not exist !!");
       }
  }
}
