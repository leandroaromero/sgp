<?php

/**
 * Imagen form.
 *
 * @package    museo
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class ImagenAdminForm extends BaseImagenForm
{
  public function configure()
  {
   unset($this['updated_at'],$this['updated_by'],$this['created_at'],$this['created_by']);
   $this->widgetSchema['sitio_id']=  new sfWidgetFormInputHidden();
   $this->widgetSchema['descripcion']=  new sfWidgetFormTextarea();
   $dir= sfContext::getInstance()->getUser()->getFile(); 
   
   $this->widgetSchema['archivo'] = new sfWidgetFormInputFileEditable(array(
   'label'     => 'Imagen *',
   'file_src'  => '/uploads/'.$dir.'/fotos/'.$this->getObject()->getArchivo(),
   'is_image'  => true,
   'edit_mode' => false,
   'template'  => '<div>%file%<br /><label></label>%input%<br /><label></label>%delete% Eliminar imagen actual</div>',
));
    
$this->validatorSchema['archivo'] = new sfValidatorFile(array(
   'required'   => false,
   'mime_types' => 'web_images',
   'path' => sfConfig::get('sf_upload_dir').'/'.$dir.'/fotos/',
   'validated_file_class' => 'sfResizedFile',
));
	  
  }
//   public function doSave($con = null)
//{
//     foreach ($this->getValue('Imagen') as $pic)
//  {
//    $picture = Doctrine::getTable('Imagen')->find($pic['id']);
//    $fail=$picture->getArchivo();
//    if(isset($pic['archivo'])){
//   $dir= sfContext::getInstance()->getUser()->getFile(); 
//    $p  = sfConfig::get('sf_upload_dir').'/'.$dir.'/fotos/'.$fail;
//    $p1 = sfConfig::get('sf_upload_dir').'/'.$dir.'/fotos/big/'.$fail;
//    $p2 = sfConfig::get('sf_upload_dir').'/'.$dir.'/fotos/thumbs/'.$fail;
//
//      @unlink($p);
//      @unlink($p1);
//      @unlink($p2);
//  }
//
//  }
//  return parent::doSave($con);
//}


}
