<ul class="sf_admin_actions">
<?php if ($form->isNew()): ?>   
  <li class="sf_admin_action_cancel">
  <?php echo link_to(__('Cancel', array(), 'messages'), 'imagen/List_cancel?o_id='.$form->getObject()->getSitioId(), array()) ?>
  </li>
  <?php echo $helper->linkToSave($form->getObject(), array(  'params' =>   array(  ),  'class_suffix' => 'save',  'label' => 'Save',)) ?>
<?php else: ?>
  <?php echo link_to(image_tag('cross.png', array('alt_title' => 'Eliminar imagen', 'height' => 16)).'Delete', 'imagen/delete?id='.$form->getObject()->getId(), array('method' => 'delete', 'confirm' => 'Are you sure?')) ?>
   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  <li class="sf_admin_action_cancel">
  <?php echo link_to(__('Terminar', array(), 'messages'), 'imagen/List_cancel?o_id='.$form->getObject()->getSitioId(), array()) ?>
  </li>
  <li>
    <?php echo link_to(__('Descargar', array(), 'messages'), 'imagen/download?i_id='.$form->getObject()->getId(), array()) ?>
  </li>  
  <?php echo $helper->linkToSave($form->getObject(), array(  'params' =>   array(  ),  'class_suffix' => 'save',  'label' => 'Save',)) ?>

<?php endif; ?>
</ul>
