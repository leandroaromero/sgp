<?php

require_once dirname(__FILE__).'/../lib/mapasGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/mapasGeneratorHelper.class.php';

/**
 * mapas actions.
 *
 * @package    sgp
 * @subpackage mapas
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class mapasActions extends autoMapasActions
{
      public function executeIndex(sfWebRequest $request)
  {
    // sorting
    if ($request->getParameter('sort') && $this->isValidSortColumn($request->getParameter('sort')))
    {
      $this->setSort(array($request->getParameter('sort'), $request->getParameter('sort_type')));
    }

    // pager
    if ($request->getParameter('page'))
    {
      $this->setPage($request->getParameter('page'));
    }

		$this->pager = $this->getPager();
		$this->sort = $this->getSort();
		$this->gMap = new GMap();
        
	foreach ($this->pager->getResults() as $i => $sitio){     
        $lat  = $sitio->getLatitudUbicacion();
        $long = $sitio->getLongitudUbicacion();
        $info_window = new GMapInfoWindow('<div>
                                                <p><b> '.$sitio->getIdentificacion().'</b></p>
                                                <p> Latitud S '.$lat.'</p>
                                                <p>Longitud W '.$long.' <p>'
                                                .$sitio->getDescripcionUbicacion().'
                                                <a href=mapas/'.$sitio->getId().'/edit >Ver mas ...</a>  </div>'
                );
        
        if( $long != "" ){
			$marker = new GMapMarker($lat,$long);
            $marker->addHtmlInfoWindow($info_window);
            $this->gMap->addMarker($marker);      
            
		}
    } 
     
        $this->gMap->centerAndZoomOnMarkers();
    
  }
    public function executeMapa(sfWebRequest $request){
        //$this->forward404unless($request->isXmlHttpRequest());
        $lat = $request->getParameter("lat");
        $long = $request->getParameter("long");
        $this->gMap = new GMap();
        $info_window = new GMapInfoWindow('<div><p> Latitud S '.$lat.'</p><p>Longitud W '.$long.' <p></div>');

        $marker = new GMapMarker($lat,$long, array('title' => '"Darwin"'));
        $marker->addHtmlInfoWindow($info_window);
        $this->gMap->addMarker($marker);

        $this->gMap->centerAndZoomOnMarkers();
    }  
    public function executeGooglemapas(sfWebRequest $request){
		
		}
}
