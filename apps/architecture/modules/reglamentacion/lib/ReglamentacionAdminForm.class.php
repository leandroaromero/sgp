<?php

/**
 * Reglamentacion form.
 *
 * @package    sgp
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class ReglamentacionAdminForm extends BaseReglamentacionForm
{
  public function configure()
  {
       $this->widgetSchema['sitio_list']->setOption('renderer_class', 'sfWidgetFormSelectDoubleList');
       $this->widgetSchema['enlace_reglamentacion']->setAttribute('style','width:600px');
       $this->widgetSchema['resumen_reglamentacion']->setAttribute('style','width:400px');
        //array('style'=> 'width:500px')
  }
}
