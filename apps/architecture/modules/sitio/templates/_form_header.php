<script type="text/javascript">
var names = <?php print_r($form['otros_nombres']->count())?>;

function addNames(num) {
  var r = $.ajax({
    type: 'GET',
    url: '<?php echo url_for('complementos/addNamesForm')?>'+'<?php echo ($form->getObject()->isNew()?'':'?id='.$form->getObject()->getId()).($form->getObject()->isNew()?'?names=':'&names=')?>'+num,
    async: false
  }).responseText;
  return r;
}
$().ready(function() {
  $('button#add_otros_nombres').click(function() {
    $("#extra_otros_nombres").append(addNames(names));
    names = names + 1;
  });
});
</script>

<script type="text/javascript">
var descriptions = <?php print_r($form['otros_nombres']->count())?>;

function addDescription(num) {
  var r = $.ajax({
    type: 'GET',
    url: '<?php echo url_for('complementos/addDescriptionForm')?>'+'<?php echo ($form->getObject()->isNew()?'':'?id='.$form->getObject()->getId()).($form->getObject()->isNew()?'?descriptions=':'&descriptions=')?>'+num,
    async: false
  }).responseText;
  return r;
}
$().ready(function() {
  $('button#add_description').click(function() {
    $("#extra_description").append(addDescription(descriptions));
    descriptions = descriptions + 1;
  });
});
</script>

<script type="text/javascript">
var intervenciones = <?php print_r($form['otros_nombres']->count())?>;

function addInter(num) {
  var r = $.ajax({
    type: 'GET',
    url: '<?php echo url_for('complementos/addIntervencionForm')?>'+'<?php echo ($form->getObject()->isNew()?'':'?id='.$form->getObject()->getId()).($form->getObject()->isNew()?'?inter=':'&inter=')?>'+num,
    async: false
  }).responseText;
  return r;
}
$().ready(function() {
  $('button#add_intervencion').click(function() {
    $("#extra_intervencion").append(addInter(intervenciones));
    intervenciones = intervenciones + 1;
  });
});
</script>
<script>
    function searchNumber( id ){
             var   valores = $.ajax({
                   type: 'GET',
                   url: '<?php echo url_for('complementos/getNumber')?>'+'?id='+id,
                   async: false
                }).responseText;
   document.getElementById('sitio_orden_identificacion').value= valores.substring(1, valores.length-1);
     
    }
</script>
