<?php

/**
 * complementos actions.
 *
 * @package    sgp
 * @subpackage complementos
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class complementosActions extends sfActions
{
 /**
  * Executes index action
  *
  * @param sfRequest $request A request object
  */
  public function executeIndex(sfWebRequest $request)
  {
    $this->forward('default', 'module');
  }
  public function executeAddNamesForm(sfWebRequest $request){
         $this->forward404unless($request->isXmlHttpRequest());
         $number = intval($request->getParameter("names"));

         if($sitio =    Doctrine::getTable('Sitio')->find($request->getParameter('id'))){  
             $form = new SitioAdminForm($sitio);
             }else{
             $form = new SitioAdminForm(null);
         }

         $form->addOtrosNombres($number);
         return $this->renderPartial('addOtrosNombres',array('form' => $form, 'num' => $number));
  }
  public function executeAddDescriptionForm(sfWebRequest $request){
         $this->forward404unless($request->isXmlHttpRequest());
         $number = intval($request->getParameter("descriptions"));

         if($sitio =    Doctrine::getTable('Sitio')->find($request->getParameter('id'))){  
             $form = new SitioAdminForm($sitio);
             }else{
             $form = new SitioAdminForm(null);
         }

         $form->addDescriptionGeneral($number);
         return $this->renderPartial('addDescription',array('form' => $form, 'num' => $number));
  }
  public function executeAddIntervencionForm(sfWebRequest $request){
         $this->forward404unless($request->isXmlHttpRequest());
         $number = intval($request->getParameter("inter"));

         if($sitio =    Doctrine::getTable('Sitio')->find($request->getParameter('id'))){  
             $form = new SitioAdminForm($sitio);
             }else{
             $form = new SitioAdminForm(null);
         }

         $form->addIntervencion($number);
         return $this->renderPartial('addIntervencion',array('form' => $form, 'num' => $number));
  }
  
  
      public function executeGetNumber(sfWebRequest $request){
         $id = $request->getParameter('id');

             $sitios = Doctrine_Query::create()
              ->from('Sitio s')
              ->whereIn('s.departamento_id', $id)
              ->orderBy('s.orden_identificacion Desc')  
              ->execute();
         $bandera= true;
         $valorC="1";
         $cantidad=count($sitios);
         $tope='';
         foreach ($sitios as $sitio){
              if( $cantidad == $sitio->getOrdenIdentificacion() && $bandera)
                  {
                    $valorC = $sitio->getOrdenIdentificacion(); 
                    $valorC++;
                    return $this->renderText(json_encode($valorC.'' ));
                  }else{
                      if($bandera){
                       $valorC=  $sitio->getOrdenIdentificacion() +1 ;
                    //   $valorC=  $valorC.'; ';
                       $tope=$sitio->getOrdenIdentificacion();
                      }else{
                          while ( $tope > $sitio->getOrdenIdentificacion() ){
                            $valorC.=  '; '.$tope;
                            $tope--;
                          }
                      }
                  }
               
             //$tope=$sitio->getOrdenIdentificacion();     
             $bandera= false;
             $tope--;
              	
      }
          while ( $tope > 0 ){
                            $valorC.=  '; '.$tope;
                            $tope--;
                          }   
        
        return $this->renderText(json_encode($valorC));
    }
  
}
