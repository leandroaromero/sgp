<?php use_helper('I18N', 'Date') ?>
<?php include_partial('mapas/assets') ?>

<div id="sf_admin_container">
  <h1><?php echo __('Sitios', array(), 'messages') ?></h1>

  <?php include_partial('mapas/flashes') ?>

  <div id="sf_admin_header">
    <?php include_partial('mapas/list_header', array('pager' => $pager)) ?>
  </div>

  <div id="sf_admin_bar">
    <?php include_partial('mapas/filters', array('form' => $filters, 'configuration' => $configuration)) ?>
  </div>

  <div id="sf_admin_content">

<?php use_helper('GMap') ?>
<?php include_map($gMap,array('width'=>'750px','height'=>'650px')); ?>
<?php include_map_javascript($gMap); ?>
  </div>

  <div id="sf_admin_footer">
    <?php include_partial('mapas/list_footer', array('pager' => $pager)) ?>
  </div>
</div>
