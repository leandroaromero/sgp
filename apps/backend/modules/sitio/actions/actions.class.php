<?php

require_once dirname(__FILE__).'/../lib/sitioGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/sitioGeneratorHelper.class.php';

/**
 * sitio actions.
 *
 * @package    sgp
 * @subpackage sitio
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class sitioActions extends autoSitioActions
{
    public function executeMapa(sfWebRequest $request){
        //$this->forward404unless($request->isXmlHttpRequest());
        $lat = $request->getParameter("lat");
        $long = $request->getParameter("long");
        $this->gMap = new GMap();
        $info_window = new GMapInfoWindow('<div><p> Latitud S '.$lat.'</p><p>Longitud W '.$long.' <p></div>');

        $marker = new GMapMarker($lat,$long, array('title' => '"Darwin"'));
        $marker->addHtmlInfoWindow($info_window);
        $this->gMap->addMarker($marker);

        $this->gMap->centerAndZoomOnMarkers();
    }
}

