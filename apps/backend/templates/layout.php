<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
  <head>
    <?php include_http_metas() ?>
    <?php include_metas() ?>
    <?php include_title() ?>
    <link rel="shortcut icon" href="/favicon.ico" />
    <?php include_stylesheets() ?>
    <?php include_javascripts() ?>
    <?php // use_helper('GMap') ?>
<!--     <script src="http://maps.google.com/maps?file=api&amp;v=2&amp;sensor=false&amp;key=ABQIAAAAoYRV1ylFpnWqYyULiP8FhBTOVvkJcXYGbYpE83gfMzPGmQWp3BRkx2EWfDJTV8p0xtiiBDURJ48FiA" type="text/javascript"></script>-->
    <?php // use_helper('JavascriptBase','GMap') ?>
  </head>
  <body>
    
    
      <?php include_component('sfAdminDash','header'); ?>
      <?php echo $sf_content ?>
      <?php  include_partial('sfAdminDash/footer'); ?>
    
      
  </body>
</html>
