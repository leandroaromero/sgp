<?php

class myUser extends sfBasicSecurityUser
{
      
  /**
   * @var string
   */
  public $base;
  /**
   * @var string
   */
  public $base_datos;
  

  public function setDataBase($base)
  {
      
      $this->base= $this->getAttribute('base_dato');//$base;
      
  }

  
    public function getDataBase(){
    $base= $this->getAttribute('base_dato');

    $default = 'architecture' ; //sfConfig::get('app_institucion_default');

       if( is_null($base) &&  !$this->isAuthenticated() ){
            $base= $default;
            $this->setAttribute('base_dato', $default);
            }
            return $base;
      }
}