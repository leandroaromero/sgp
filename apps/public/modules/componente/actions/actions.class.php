<?php

/**
 * componente actions.
 *
 * @package    sgp
 * @subpackage componente
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class componenteActions extends sfActions
{
 /**
  * Executes index action
  *
  * @param sfRequest $request A request object
  */
  public function executeIndex(sfWebRequest $request)
  {
    $this->forward('default', 'module');
  }

  public function executeGetCategorias($request)
  {
      $this->getResponse()->setContentType('application/json');
      
      $query = Doctrine::getTable('Categoria')->createQuery('a');
      $caracteristicas=array();
      foreach ($query->execute() as $key => $value) {
        $ambito=array();
        $ambito['nombre']=$value->getNombre();
        $caracteristicas[]=$ambito; 
      }

      return $this->renderText(json_encode( $caracteristicas));
  }
 public function executeGetSitio($request)
  {
      $this->getResponse()->setContentType('application/json');

      $this->forward404Unless($objeto = Doctrine_Core::getTable('Sitio')->find(array($request->getParameter('id'))), sprintf('Object public_object does not exist (%s).', $request->getParameter('id'))); 

      return $this->renderText(json_encode( $objeto->getArrayString()));
  }


  /*
        general = string de lo que escribio el usuario
        categorias =ambitos
        cantidad =cantidad de resultados por pagina
        pagina =pagina devuelta
*/
  public function executeGetSitios($request)
  {
      $this->getResponse()->setContentType('application/json');

      $query = Doctrine::getTable('Sitio')->retrieveSearchSelect(
                  $request->getParameter('general'),
                  $request->getParameter('categorias')
      );

      $manifestaciones= $this->armarRespuesta($query,
                        $request->getParameter('cantidad', 10),
                        $request->getParameter('pagina', 1) 
      );
      return $this->renderText(json_encode($manifestaciones));
  }

  protected function armarRespuesta($query, $cantidad, $pagina)
  {
      $pager = new sfDoctrinePager( 'Sitio', $cantidad);
      $pager->setQuery($query);
      $pager->setPage($pagina);
      $pager->init();
      $dato = array();

      $dato['total']= $pager->getNbResults();
      if ($pager->haveToPaginate()): 
        $dato['pagina']=  $pager->getPage(); 
        $dato['pagina_total']=  $pager->getLastPage();
      endif; 

      $resultado= array();
      $resultado[]= $dato;
      foreach ($pager->getResults() as $key => $sitio) {
        $resultado[]= $sitio->getArrayString();
      }
      return $resultado;
  }
}
