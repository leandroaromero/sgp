<?php

/**
 * mapas actions.
 *
 * @package    sgp
 * @subpackage mapas
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class mapasActions extends sfActions
{
 /**
  * Executes index action
  *
  * @param sfRequest $request A request object
  */
  public function executeIndex(sfWebRequest $request)
  {
      $this->getUser()->setAttribute('base_dato', 'architecture');
      $this->form= new SitioAdminFormFilter();
      $conn= Doctrine_Manager::getInstance()->getConnection('architecture');

              $q = Doctrine_Query::create($conn)
                   ->from('Sitio s')  ;       
              $this->pager = new sfDoctrinePager( 'Sitio', 8);
              $this->pager->setQuery($q);
              $this->pager->setPage($request->getParameter('page', 1));
              $this->pager->init();
      
  }

  
  public function executeSearch(sfWebRequest $request)
  {
      
      ini_set("max_execution_time",0);
      ini_set('memory_limit', '-1');
      
      $cambio = $request->getParameter('search_places');      
    
      if( $request->getParameter('first') == 'false'){
        $cambio = $this->getUser()->getAttribute('search_places');  
      }else{
        $this->getUser()->setAttribute('search_places', $cambio);
      }

      $conn= Doctrine_Manager::getInstance()->getConnection('architecture');
      $q = Doctrine_Query::create($conn)
                   ->from('Sitio s');
                  if ( isset($cambio['general'])){
                    $q->where('s.identificacion like ?', '%'.$cambio['general'].'%');
                  }
                  
                  /* ->leftJoin('s.Departamento d')   
                   ->leftJoin('s.OtroNombre o')  
                   ->leftJoin('s.SitioCategoria sc')
                   ->leftJoin('sc.Categoria c');
                     if($first){
                        $search= $buscar;  
                        $q= $this->getQuerySearchFirst($q, $search );
                        
                      } else{
                          //primera prueba
                       //   $q=  $this->getQuerySearchSecond($q, $b);
                      }
                           $q->groupBy('s.id');
                    */       
              $this->pager = new sfDoctrinePager( 'Sitio', 8);
              $this->pager->setQuery($q);
              $this->pager->setPage($request->getParameter('page', 1));
              $this->pager->init();
          
  }  
  
  
  public function armarBusqueda($buscar){
      $busquedas=  $this->multiexplode(array(",",".","|",":"," ",";","+"), $buscar);
     if(empty($busquedas))
        $busquedas[]='';
      return $busquedas;
  }  
  
  function multiexplode ($delimiters,$string) {  
    $ready = str_replace($delimiters, $delimiters[0], $string);
    $launch = explode($delimiters[0], $ready);
    return  array_filter($launch);
}
  
   function getQuerySearchFirst( $q, $buscar){
           $banderas= array(); 
               foreach ($buscar as $bi ){ 
                        if(! in_array('nombre', $banderas)){
                             $q->where('s.identificacion like ?', '%'.$bi.'%');
                             $banderas[]='nombre';
                        }else{
                             $q->andWhere('s.identificacion like ?', '%'.$bi.'%');
                        }  
                     }
                        ///////////////////////////////
                     
                     foreach ($buscar as $bi ){    
                        if(! in_array('descripcion', $banderas)){
                             $q->orWhere('s.descripcion_ubicacion like ?', '%'.$bi.'%') ;
                             $banderas[]='descripcion';
                        }else{     
                             $q->andWhere('s.descripcion_ubicacion like ?', '%'.$bi.'%');
                        }
                     }  
                        //////////////////////////////////             
                     
                     foreach ($buscar as $bi ){    
                        if(! in_array('departamento', $banderas)){
                              $q->orWhere('d.nombre like ?', '%'.$bi.'%');
                              $banderas[]='departamento';        
                        }else{
                            $q->andWhere('d.nombre like ?', '%'.$bi.'%');
                        }      
                     }  
                        //////////////////////////////////                                              
                               
                     
                     foreach ($buscar as $bi ){    
                        if(! in_array('categoria', $banderas)){
                              $q->orWhere('c.nombre like ?', '%'.$bi.'%');
                              $banderas[]='categoria';        
                        }else{
                            $q->andWhere('c.nombre like ?', '%'.$bi.'%');
                        }      
                     }  
                        //////////////////////////////////                                              
                               
                     
                     foreach ($buscar as $bi ){    
                        if(! in_array('otro_nombre', $banderas)){
                              $q->orWhere('o.otro_nombre like ?', '%'.$bi.'%');
                              $banderas[]='otro_nombre';        
                        }else{
                            $q->andWhere('o.otro_nombre like ?', '%'.$bi.'%');
                        }      
                     }
                     return $q;
   }
   
   function getQuerySearchSecond($q, $search){
                     $q->orwhere('s.identificacion like ?', '%'.$search.'%')
                       ->orWhere('c.descripcion_ubicacion like ?', '%'.$search.'%')
                       ->orWhere('c.nombre like ?', '%'.$search.'%')
                       ->orWhere('o.otro_nombre like ?', '%'.$search.'%');  
                        
       return $q;
   }
    
  public function executePaginar($request) {
      
      $this->sites  = $this->getUser()->getAttribute('result_s');
      $this->search   = $this->getUser()->getAttribute('search_more');
      
      $pager=$request->getParameter('page');
      $this->getUser()->setAttribute('pagination', $pager);
      $this->sites_pa=  new Paginar($this->sites,12,$pager);
      $this->setTemplate('search');
      
  } 
 
  public function executeResult(sfWebRequest $request) 
  {
    $conn = Doctrine_Manager::getInstance()->getConnection('architecture');
    $this->sitio= Doctrine_Query::create($conn)
            ->from('Sitio c')
            ->where('c.id = ?', $request->getParameter('sitio_id') )
            ->fetchone(); 

    $this->carpeta='architecture';         
    $this->setLayout('layout_mapa');         

  }   
    
  public function executeChangeLanguage(sfWebRequest $request)
   {
         $form = new sfFormLanguage( $this->getUser(), array('languages' => array('en', 'es')) );
         $form->process($request);
         //$this->getUser()->aumentarContador('languages');
         return $this->redirect('homepage');
}
  
}
