<?php

/**
 * Sitio filter form.
 *
 * @package    sgp
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class SitioAdminFormFilter extends BaseSitioFormFilter
{
  public function configure()
  {
      unset($this['texto_ubicacion'],$this['categoria_list']);
      
        $parameter = sfConfig::get('app_institucion_data', array(''=>'') );
        sfProjectConfiguration::getActive()->loadHelpers(array('I18N'));
        $choices    = $parameter; //array('' => '') + $parameter; 

        $this->widgetSchema['museo'] = new sfWidgetFormChoice(
                                                                array(
                                                                    'choices'   => $choices,
                                                                    'multiple'  => false,
                                                                    'label'     => __('Places'),
                                                                    'expanded'  => false,
                                                                    ),
                                                                array( 'style'=>'width:360px;height:27px;font-size:17px')
                                                            );
        $this->widgetSchema['general']= new sfWidgetFormInput(array(
                                                                    'label'=>__('Search')
                                                                    ),array( 'style'=>'width:360px;height:25px;font-size:17px'));
        
        $this->widgetSchema->setNameFormat('search_places[%s]'); 
        
  }
}
