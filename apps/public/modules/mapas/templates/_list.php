
<table>

    <thead>
        <tr>
            <th>
                <?php echo __('Location')?>
            </th>
            <th>
                <?php echo __('Denomination')?>
            </th>
            <th>
                <?php echo __('Cataloging')?>
             </th>
             <th>
                <?php echo __('Other name')?>
             <th>   

            </th>
    </thead>
        <?php foreach ($places as $place): ?>
            <tr>              
                <td>
                    <?php echo $place->getLocalidad() ?> <br/>
                    <b> <?php echo $place->getDepartamento() ?></b> 
                </td>  
                <td>
                    <b> <?php echo $place->getIdentificacion() ?></b> 
                </td>
                <td>
                    <?php foreach ($place->getCategoria() as $key => $value):?>
                        <?php echo $value?><br/>
                    <?php endforeach?>
                </td>
                <td>
                    <?php echo $place->getOtroNombres() ?>                    
                </td>        
                    
                <td> 
                    <?php
                        echo link_to(image_tag('public/img/search-submit.png'),'mapas/result?sitio_id='.$place->getId(),
                                array('popup'=>array('popupWindow','status=no,location=no,resizable=yes,width=850, height=600,left=300,top=0'), 'size=100x100'))
                            ?>
                     </td>   
                </tr>
         <?php  endforeach;?>
</table>







