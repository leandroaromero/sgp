<style type="text/css">
#portfolio{
	display:block;
	width:100%;
	line-height:1.6em;
	}

#portfolio {
	display:block;
	width:100%;
	margin-bottom:20px;
	padding:0;
	border-bottom:0.1px solid #ffffff;
	}



#portfolio ul, #portfolio h2, #portfolio p, #portfolio img{
	margin:0;
	padding:0;
	list-style:none;
	}

#portfolio  li{
	float:left;
	margin:0 20px 20px 0;
	}

#portfolio  li.last{
	margin-right:0;
	}

#portfolio  li img{
	border:5px solid #DFDFDF;
	}

#portfolio .fl_right p.name{
	font-weight:bold;
	}

#portfolio .fl_right p.readmore{
	text-transform:uppercase;
	}
</style>


<div class="sf_admin_list">
  
    <table cellspacing="0">

      <tbody>
      <div id="portfolio">

          <ul>
           <?php  while($objeto = $result->fetchPagedRow()):  ?>
            
            
              <li style="list-style:none"  >
                  <div align="center" style="background-color: #DFDFDF; min-height: 150px; min-width: 210px">
               <?php if($foto = $objeto->getImagen1()){
                            $dir= $objeto->getFile();
                            echo link_to( image_tag('../uploads/'.$dir.'/fotos/'.$foto, 'style=" height: 150px; max-width: 210px"'),'gallery/result?index='.$objeto->getIndex(),
                                    array('popup'=>array('popupWindow','status=no,location=no,resizable=yes,width=850,height=600,left=320,top=0'), ''));
                        }else{
                                echo link_to( image_tag('images/demo/210x150.gif'),'gallery/result?index='.$objeto->getIndex(),
                                    array('popup'=>array('popupWindow','status=no,location=no,resizable=yes,width=850,height=600,left=320,top=0'), ''));
                        }
                 ?>

              </div>
              <p class="name"><?php echo $objeto->getNombreTitulo() ?></p>
              <p class="readmore">
                   <p>
                  <div class="fb-like" data-href="http://patrimonio.chaco.gov.ar/gallery/<?php echo $objeto->getFile()?>/<?php echo $objeto->getId()?>" data-layout="button_count" data-action="like" data-show-faces="false" data-share="false"></div>
              
                <ul class="sf_admin_td_actions">
                    <li class="sf_admin_action_show">
                      <?php //echo link_to(__('Visualizar', array(), 'messages'), 'objeto/ListShow?id='.$objeto->getId(), array()) ?>
                    </li>
                    
               </ul>
              </p>
             
            </li>

             
           <?php endwhile; ?>
          
          </ul>
        </div>

      
                <tr>
                    <td><?php echo $result->getTotal().' Resultados - ( página '.$result->getPageNumber().' / '.$result->fetchNumberPages().' )' ?></td>
                    
                </tr>
      </tbody>
    </table>

</div>
<script type="text/javascript">
/* <![CDATA[ */
function checkAll()
{
  var boxes = document.getElementsByTagName('input'); for(var index = 0; index < boxes.length; index++) { box = boxes[index]; if (box.type == 'checkbox' && box.className == 'sf_admin_batch_checkbox') box.checked = document.getElementById('sf_admin_list_batch_checkbox').checked } return true;
}
/* ]]> */
</script>
