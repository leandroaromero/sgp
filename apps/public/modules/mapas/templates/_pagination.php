
<?php use_helper('jQuery'); ?>
<!-- pagination --> 
<div class="clear"></div>

<div id="to-top">

<ul class='pager'>
    <li class='first-page' >
        <?php echo jq_link_to_remote('<<', 
            $options = array(
                'update' => 'CargarResultado',
                'url'    => 'mapas/search?page=1&first=false',     
                
            ), array(
                 'href' => '#posts'   
            )
        );
            
        ?>
    </li>
    <li>            
        <?php echo jq_link_to_remote('<', 
            $options = array(
            'update' => 'CargarResultado',
            'url'    => 'mapas/search?page='.$pager->getPreviousPage().'&first=false'),
             array(
                 'href' => '#posts',   
            )

        );
            ?>
    </li>
    <?php foreach ($pager->getLinks() as $page): ?>
        <?php if ($page == $pager->getPage()): ?>
            <li class='active'> <a> <?php echo $page ?></a></li>
        <?php else: ?>
            <li>
                <?php echo jq_link_to_remote( $page, 
                    $options = array(
                       'update' => 'CargarResultado',
                       'url'    => 'mapas/search?page='. $page .'&first=false'),
                array(
                    'href' => '#posts',   
            ));
                ?>   
            </li>
        <?php endif; ?>
    <?php endforeach; ?>
    <li>
        <?php echo jq_link_to_remote( '>', 
            $options = array(
               'update' => 'CargarResultado',
               'url'    => 'mapas/search?page='. $pager->getNextPage() .'&first=false'),
             array(
                 'href' => '#posts',   
            ));
        ?>  
    </li>
    <li class='last-page'>
        <?php echo jq_link_to_remote( '>>', 
            $options = array(
               'update' => 'CargarResultado',
               'url'    => 'mapas/search?page='. $pager->getLastPage() .'&first=false'),
             array(
                 'href' => '#posts',   
            ));
        ?>  
    </li>
</ul>
                        <!-- ENDS pagination -->
</div>

                    
                    

