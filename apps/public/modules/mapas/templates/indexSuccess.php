
<?php echo use_stylesheet('ui-lightness/jquery-ui-1.8.11.custom.css')?>
<?php echo use_stylesheet('jquery-ui-1.8.7.custom.css')?>
<?php echo use_stylesheet('layout.css')?>
<?php echo use_stylesheet('main.css')?>
<?php echo use_javascript('jquery-1.4.4.min.js')?>  
<?php echo use_javascript('jquery-ui-1.8.11.custom.min.js')?>        
<?php use_helper('jQuery'); ?>        
        



<!-- title -->
    <div id="page-title">
        <span class="title"><?php echo __('Patrimonio')?></span>
        <span class="subtitle"><?php echo __('Material')?></span>
    </div>
    <div id="posts">      
        <form action="javascript:void(0);" method="post" >
          <center>
            <table class="table">
              <tbody>
                <tr>
                    <td style="border-bottom: 0px">
                        <?php echo $form['general']->renderLabel() ?>
                    </td>
                    <td style="border-bottom: 0px">    
                        <?php echo $form['general']->renderError() ?></br>
                        <?php echo $form['general']->render(array('class' => 'number')) ?>
                    </td>
                    <td colspan="" style="border-bottom: 0px">
                    <br/>
                    <?php echo jq_submit_to_remote( ' __()'   , __('Search'),
                        $options = array(
                        'update' => 'CargarResultado',
                        'url'    => 'mapas/search?',
                      ), 
                      $options_html = array()
                    )?>
                  </td>   

                </tr>                
              </tbody>
            </table>
          </center>
        </form>
    </div>
    <!-- ENDS title -->
    
    <div id="CargarResultado">
      <div id="resultado">
      <?php include_partial('mapas/list', array('places' => $pager->getResults())) ?>

      <div class="clear"></div>
      <div id="to-top">
        <?php if ($pager->haveToPaginate()): ?>
          <?php include_partial('mapas/pagination', array('pager' => $pager)) ?>
        <?php endif; ?>
      </div>  
      <?php echo format_number_choice('[0] no result|[1] 1 result|(1,+Inf] %1% results', array('%1%' => $pager->getNbResults()), $pager->getNbResults(), 'sf_admin') ?>
      <?php if ($pager->haveToPaginate()): ?>
        <?php echo __('(page %%page%%/%%nb_pages%%)', array('%%page%%' => $pager->getPage(), '%%nb_pages%%' => $pager->getLastPage()), 'sf_admin') ?>
      <?php endif; ?>
      </div>
    </div>        