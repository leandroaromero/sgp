    <!-- title -->
    <div id="page-title" style="">
            <span class="title" style="font-size:30px;"><?php echo $sitio->getIdentificacion();?></span>
    </div>
        <div id="content" style="margin-left: 9%">        
           <div class="">
               <div>
                    <table style="border-top: 0px">
                      <tbody>                        
                        <?php if ($sitio->getCategorias()):?> 
                          <tr>
                            <td style="width: 130px;  border-bottom: 0px" >
                              <b><?php echo __('Cataloging')?>:</b>
                            </td>
                            <td style="border-bottom: 0px" colspan="2">
                              <span class="subtitle">
                                <?php foreach ($sitio->getCategoria() as $key => $value):?>
                                  <?php echo $value->getNombre();?></br> 
                                <?php endforeach ?>                            
                              </span>
                            </td>
                          </tr>
                        <?php endif?>  

                        <?php if ($sitio->getOtroNombres()):?> 
                          <tr>
                            <td style="width: 130px;  border-bottom: 0px ">
                              <b><?php echo __('Other name')?>:</b>
                            </td>
                            <td style="border-bottom: 0px " colspan="2">
                              <span class="subtitle">
                                <?php foreach ($sitio->getOtroNombre() as $key => $value):?>      
                                  <?php echo $value->getOtroNombre().' '?>                              
                                  <b><?php echo __('Period')?>:</b> <?php echo $value->getPeriodo()?></br>
                                <?php endforeach ?>                            
                              </span>                          
                            </td>  
                          </tr>
                        <?php endif?>  
                          <tr>
                            <td style="width: 130px;  border-bottom: 0px ">
                              <b><?php echo __('Location')?>:</b>
                            </td>
                            <td style="border-bottom: 0px ">
                              <span class="subtitle">
                                  <?php echo $sitio->getDepartamento().' - '.$sitio->getLocalidad() ?>
                              </span>
                            </td>
                            <td style="border-bottom: 0px ">
                              <span class="subtitle">
                                <?php echo __('Latitude')?> <?php echo $sitio->getLatitudUbicacion();?></br>
                                <?php echo __('Longitud')?> <?php echo $sitio->getLongitudUbicacion(); ?>
                              </span>
                            </td>
                          </tr>                          
                        <?php if ($sitio->getTextoUbicacion()):?> 
                          <tr>
                            <td style="width: 130px;  border-bottom: 0px ">
                              <b><?php echo __('Address')?>:</b>
                            </td>
                            <td style="border-bottom: 0px " colspan="2">
                              <span class="subtitle">
                                  <?php echo $sitio->getTextoUbicacion();?></br>
                              </span>
                            </td>
                          </tr>
                        <?php endif?> 
                        <?php if ($sitio->getDescripcionUbicacion()):?> 
                          <tr>
                            <td style="width: 130px;  border-bottom: 0px ">
                            </td>
                            <td style="border-bottom: 0px " colspan="2">
                              <span class="subtitle">
                                  <?php echo html_entity_decode($sitio->getDescripcionUbicacion());?></br> 
                              </span>
                            </td>
                          </tr>
                        <?php endif?>  
                      </tbody>
                    </table>
               </div>
               
            </div>
            <br/>
             
            <?php echo htmlspecialchars_decode ( $sitio->getEnlace() )?>
          </br>
            
            <center>
                <div class="fb-like" data-href="http://patrimonio.chaco.gov.ar/sgp/web/public.php/mapas/<?php echo $carpeta?>/<?php echo $sitio->getId()?>" data-layout="standard" data-action="like" data-show-faces="true" data-share="true"></div>
            </center>
      </div>