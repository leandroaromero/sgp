</html>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
  <head>
    	<?php include_http_metas() ?>
    	<?php include_metas() ?>
    	<?php include_title() ?>
    	<?php $ico= sfConfig::get('app_url_ico');  ?>
    	<link rel="shortcut icon" href="<?php echo $ico ?>" />
    	<?php include_stylesheets() ?>
    	<?php include_javascripts() ?>
    	<?php $web= sfConfig::get('app_url_web');  ?>
   
		
	</head>
     
	<div id="fb-root"></div>
	<script>(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s); js.id = id;
		js.src = "//connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v2.3&appId=150592058423746";
		fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));
	</script>

    <body>                             
        
   		<script>
  			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function()
  			{
  				(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  				m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  			})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  			ga('create', 'UA-62944725-1', 'auto');
  			ga('send', 'pageview');
		</script>             
        <!-- HEADER -->
		<div id="header">
			<!-- wrapper-header -->
			<div class="wrapper">
				<a href="<?php echo url_for('@homepage')?>">
					<img id="logo" src="/sgp/<?php echo $web?>images/public/logo.png" alt="Museos De La Provincia del Chaco" />
				</a>
				<!-- search -->
				<div class="top-search">
					<form  method="get" id="searchform" action="#">
						<div>			
						</div>
					</form>
				</div>
				<!-- ENDS search -->
			</div>
			<!-- ENDS wrapper-header -->					
		</div>
		<!-- ENDS HEADER -->
			
		<!-- Menu -->
		<div id="menu">
			<!-- ENDS menu-holder -->
			<div id="menu-holder">
				<!-- wrapper-menu -->
                <div class="wrapper">
                	<!-- Navigation -->
					<ul id="nav" class="sf-menu">
                        <li id="li_home"  onclick="setMenu('li_home')" class="" >
                        	<a href="../../../"><?php echo __('Objects')?><span class="subheader"><?php echo __('Showcase work')?></span></a>
                        </li>
                        <li id="li_news"  onclick="setMenu('li_news')" class="" >
                        	<a href="../../../noticias"><?php echo __('News')?><span class="subheader"><?php echo __('and Events')?></span></a>
                        </li>

                        <li id="li_places"  onclick="setMenu('li_places')" class="" >
                        	<a href="<?php echo url_for('@homepage')?>"><?php echo __('Patrimonies')?><span class="subheader"><?php echo __('Material')?></span></a>
                        </li>

                        <li id="li_patrimonio" onclick="setMenu('li_patrimonio')">
                        	<a href="../../../patrimonio"><?php echo __('Dirección')?><span class="subheader"><?php echo __('Patrimonio')?></span></a>
                        </li>
				        <li id="li_museum"  onclick="setMenu('li_museum')" >
				        	<a href="../../../museos"><?php echo __('Our Museums')?><span class="subheader"><?php echo __('Information')?></span></a>
                            <?php $museos =Doctrine_Query::create()->from('Institution')
                                                    ->orderBy('name')
                                                    ->execute();?>
                            <ul>
                                <?php foreach ($museos as $museo):?>
                                    <li onclick="setMenu('li_museum')" >
                                        <a  href="../../../el_museo/<?php echo $museo->getId();?>" > 
                                            <span>
                                                <?php echo $museo->getName()?> 
                                                <?php if($museo->getSubtitle()):?>
                                                    " <?php echo $museo->getSubtitle()?> " 
                                                <?php endif?>
                                            </span>
                                        </a>
                                    </li>  
                                <?php endforeach;?> 
                            </ul>
                        </li>               
						<li id="li_contact" onclick="setMenu('li_contact')">
							<a href="../../../comentarios"><?php echo __('Contact')?><span class="subheader"><?php echo __('Get in touch')?></span></a>
                            <ul>   
                                <li>
                                	<a onclick="setMenu('li_contact')" href="../../../contacto"> <span><?php echo __('Contact us')?></span></a>
                                </li>
                                <li>
                                	<a onclick="setMenu('li_contact')" href="../../../comentarios"> <span><?php echo __('Comments')?></span></a>
                                </li>

                            </ul>
						</li>

                        <li id="li_inscription" onclick="setMenu('li_inscription')" class="" >
                          	<a  href="http://patrimonio.chaco.gov.ar/remus/web/index.php/inscription"><?php echo __('Inscription')?><span class="subheader"><?php echo __('Open')?></span>
                               	</a>
                        </li>                             
				    </ul>
					<!-- Navigation -->
				</div>
					<!-- wrapper-menu -->
			</div>
				<!-- ENDS menu-holder -->
		</div>
			<!-- ENDS Menu -->
        <!-- MAIN -->
        <div id="main">    
            <!-- wrapper-main -->
            <div class="wrapper">						
            <!-- content -->
	            <div id="content">
                    <div style="display: inline; float: right">
                        <?php $user = sfContext::getInstance()->getUser(); $form = new sfFormLanguage($user, array('languages' => array('en', 'es')));?>
                        <form action="<?php echo url_for('@change_language') ?>">
                            <?php echo $form ?><input type="submit" value="<?php echo __('go')?>" />
                        </form>
                    </div>
    
                    <?php echo $sf_content ?>
    	
                </div>
		        <!-- ENDS CONTENT -->  
		    </div>
			<!-- ENDS WRAPPER-->  
        </div>
		<!-- ENDS MAIN -->   
                         
		<!-- Twitter -->
		<div id="twitter" style="height: 95px;">
            <div class="wrapper" ><br/>
                <center>
                    <div class="fb-like" data-href="http://patrimonio.chaco.gov.ar" data-layout="standard" data-action="like" data-show-faces="true" data-share="true"></div> 
			    </center>  
			       
			</div>
			<!-- 	</div>-->
		</div>
		<!-- ENDS Twitter -->

			
		<!-- FOOTER -->
		<div id="footer">
			<!-- wrapper-footer -->
			<div class="wrapper">
				<!-- footer-cols -->
				<ul id="footer-cols">
					<li class="col">
						<h6><?php echo __('Pages')?></h6>
						<ul>
							<li class="page_item">
								<a href="../../../"><?php echo __('Objects')?></a>
							</li>
                            <li class="page_item">
                            	<a href="<?php echo url_for('homepage')?>"><?php echo __('Patrimonies')?></a>
                            </li>
							<li class="page_item">
								<a href="../../../noticias"><?php echo __('News')?></a>
							</li>
							<li class="page_item">
								<a href="../../../patrimono"><?php echo __('Dirección')?></a>
							</li>
                            <li class="page_item">
                               	<a href="../../../museos"><?php echo __('Our Museums')?></a>
                            </li>
                            <li class="page_item">
                            	<a href="../../../contacto"><?php echo __('Contact')?></a>
                            </li> 
                            <li class="page_item" onclick="setMenu('li_inscription')">
                               	<a href="http://patrimonio.chaco.gov.ar/remus/web/index.php/inscription"><?php echo __('Inscription')?></a>
                            </li>
						</ul>
					</li>
						
					<li class="col">
						<h6><?php echo __('Our Pages in facebook')?></h6>
						<ul>
                            <?php foreach ($museos as $museo):?>
                                <?php if($museo->getFacebook()): ?> 
                                    <li>
                                        <a onclick="window.open(this.href);return false;" href="<?php echo $museo->getFacebook();?>"> 
                                            <span>
                                                <?php echo $museo->getName()?>  
                                                <?php if($museo->getSubtitle()):?>
                                                    " <?php echo $museo->getSubtitle()?> " 
                                                <?php endif?>
                                            </span>
                                        </a>
                                    </li>  
                                <?php endif?>
                            <?php endforeach;?> 
						</ul>
					</li>
                    
					<li class="col">
					    <h6><?php echo __('About Us')?></h6>
                        <p align="justify"><?php echo __('The culture is not an expense, it is a strategic investment for the development and incrase of towns; the culture is a inalienable social right and the State is its guarantor delegated...')?></p>
                        <p align="justify"><?php echo __('The doers of the culture, tha peoples and citizens of this province are the same, although governments change')?>.</p>
					</li>
						
				</ul>
				<!-- ENDS footer-cols -->
			</div>
			<!-- ENDS wrapper-footer -->
		</div>
		<!-- ENDS FOOTER -->
		
		<!-- Bottom -->
		<div id="bottom">
			<!-- wrapper-bottom -->
			<div class="wrapper">
                <div id="bottom-text"><a href="../../../componentes/about">Desarrollado por...</a></div>
				<!-- Social -->
				<ul class="social ">
                    <li></li>
                    2011 Patrimonio Chaco all rights reserved. Design http://www.luiszuno.com TEMPLATED
				</ul>
					<!-- ENDS Social -->
				<div id="to-top" class="poshytip" title="<?php echo __('To top')?>"></div>
			</div>
			<!-- ENDS wrapper-bottom -->
		</div>
		<!-- ENDS Bottom -->
                        
	</body>

</html>
