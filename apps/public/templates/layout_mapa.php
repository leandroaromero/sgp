<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
  <head>
    <?php include_http_metas() ?>
    <?php include_metas() ?>
    <?php include_title() ?>
    <?php $ico= sfConfig::get('app_url_ico');  ?>
    <link rel="shortcut icon" href="<?php echo $ico ?>" />
    <?php include_stylesheets() ?>
    <?php include_javascripts() ?>

          <!-- CSS -->

		<!--[if IE 8]>
			<link rel="stylesheet" type="text/css" media="screen" href="/museo/css/css/css/ie8-hacks.css" />
		<![endif]-->
		<!-- ENDS CSS -->	
		
		<!-- GOOGLE FONTS 
		<link href='http://fonts.googleapis.com/css?family=Ubuntu' rel='stylesheet' type='text/css'>-->
		
		<!-- JS -->

               <?php $web= sfConfig::get('app_url_web');  ?>
                <script type="text/javascript" src="/museo/<?php echo $web?>js/js/js/jquery-1.5.1.min.js"></script>
		<script type="text/javascript" src="/museo/<?php echo $web?>js/js/js/jquery-ui-1.8.13.custom.min.js"></script>
		<script type="text/javascript" src="/museo/<?php echo $web?>js/js/js/easing.js"></script>
		<script type="text/javascript" src="/museo/<?php echo $web?>js/js/js/jquery.scrollTo-1.4.2-min.js"></script>
		<script type="text/javascript" src="/museo/<?php echo $web?>js/js/js/jquery.cycle.all.js"></script>
		<script type="text/javascript" src="/museo/<?php echo $web?>js/js/js/custom.js"></script>
		
		<!-- Isotope -->
		<script type="text/javascript" src="/museo/<?php echo $web?>js/js/js/jquery.isotope.min.js"></script>
		
		<!--[if IE]>
			<script type="text/javascript" src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
		
		<!--[if IE 6]>
			<script type="text/javascript" src="/museo/<?php echo $web?>js/js/js/DD_belatedPNG.js"></script>
			<script type="text/javascript"  >
	      		/* EXAMPLE */
	      		//DD_belatedPNG.fix('*');
	    	</script>
		<![endif]-->
		<!-- ENDS JS -->
		
		
		<!-- Nivo slider -->
		
		<script src="/museo/<?php echo $web?>js/js/js/nivo-slider/jquery.nivo.slider.js" type="text/javascript"></script>
		<!-- ENDS Nivo slider -->
		
		<!-- tabs -->
		
		<script type="text/javascript" src="/museo/<?php echo $web?>js/js/js/tabs.js"></script>
  		<!-- ENDS tabs -->
  		
  		<!-- prettyPhoto -->
		<script type="text/javascript" src="/museo/<?php echo $web?>js/js/js/prettyPhoto/js/jquery.prettyPhoto.js"></script>
		<!-- ENDS prettyPhoto -->
		
		<!-- superfish -->
		
		<script type="text/javascript" src="/museo/<?php echo $web?>js/js/js/superfish-1.4.8/js/hoverIntent.js"></script>
		<script type="text/javascript" src="/museo/<?php echo $web?>js/js/js/superfish-1.4.8/js/superfish.js"></script>
		<script type="text/javascript" src="/museo/<?php echo $web?>js/js/js/superfish-1.4.8/js/supersubs.js"></script>
		<!-- ENDS superfish -->
		
		<!-- poshytip -->
		
		<script type="text/javascript" src="/museo/<?php echo $web?>js/js/js/poshytip-1.0/src/jquery.poshytip.min.js"></script>
		<!-- ENDS poshytip -->
		
		<!-- Tweet -->
		
		<script src="/museo/<?php echo $web?>js/js/js/tweet/jquery.tweet.js" type="text/javascript"></script> 
		<!-- ENDS Tweet -->
		
		<!-- Fancybox -->
		<script type="text/javascript" src="/museo/<?php echo $web?>js/js/js/jquery.fancybox-1.3.4/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
		<!-- ENDS Fancybox -->
               
		
	</head>
	<body> 
		<div id="fb-root"></div>
		<script type="text/javascript" >(function(d, s, id) 
			{
				var js, fjs = d.getElementsByTagName(s)[0];
  				if (d.getElementById(id)) return;
  				js = d.createElement(s); js.id = id;
  				js.src = "//connect.facebook.net/es_LA/all.js#xfbml=1&appId=150592058423746";
  				fjs.parentNode.insertBefore(js, fjs);
			}(document, 'script', 'facebook-jssdk'));
		</script>
                                        
		<script type="text/javascript">
			function setMenu(valor)
			{
     			var   valores = $.ajax({
                   type: 'GET',
                   url: '<?php echo url_for('componentes/setMenu')?>'+'?menu='+valor,
                   async: false
                }).responseText;     
			}

		</script>                                       
		<script language="javascript" type="text/javascript" >
			window.onload = function()
			{
     			document.getElementById('li_museum').className='';
     			document.getElementById('li_object').className='';
     			document.getElementById('li_contact').className='';	    
    			<?php if ( $sf_user->getAttribute('menu') ) :?>
            		document.getElementById('li_home').className='';
            		document.getElementById('<?php echo $sf_user->getAttribute('menu')?>').className= 'current-menu-item';
     			<?php endif;?>                   
			};
		</script>                                        
        <!-- MAIN -->
	    <div id="main">    
            <!-- wrapper-main -->
            <div class="wrapper" style="width: 800px;">						
                <!-- content -->
                <div id="content">	
                                        
                    <?php echo $sf_content ?>
                </div>
			        <!-- ENDS CONTENT -->  
		    </div>
			<!-- ENDS WRAPPER-->  
        </div>
		<!-- ENDS MAIN -->                 
	</body>
</html>

