CREATE TABLE caracteristica (id INT UNSIGNED AUTO_INCREMENT, nombre VARCHAR(50) NOT NULL, PRIMARY KEY(id)) ENGINE = INNODB;
CREATE TABLE categoria (id INT UNSIGNED AUTO_INCREMENT, nombre VARCHAR(50) NOT NULL, PRIMARY KEY(id)) ENGINE = INNODB;
CREATE TABLE departamento (id INT UNSIGNED AUTO_INCREMENT, nombre VARCHAR(50) NOT NULL, nombre_abreviado CHAR(3) NOT NULL, PRIMARY KEY(id)) ENGINE = INNODB;
CREATE TABLE descripcion_general (id INT UNSIGNED AUTO_INCREMENT, texto_descripcion VARCHAR(255) NOT NULL, sitio_id INT UNSIGNED NOT NULL, caracteristica_id INT UNSIGNED NOT NULL, INDEX sitio_id_idx (sitio_id), INDEX caracteristica_id_idx (caracteristica_id), PRIMARY KEY(id)) ENGINE = INNODB;
CREATE TABLE documentacion_inedita (id INT UNSIGNED AUTO_INCREMENT, nombre VARCHAR(100) NOT NULL, anio INT, lugar VARCHAR(100), PRIMARY KEY(id)) ENGINE = INNODB;
CREATE TABLE fuente_bibliografica (id INT UNSIGNED AUTO_INCREMENT, nombre VARCHAR(100) NOT NULL, autor_fuente_bibliografica VARCHAR(100), anio_fuente_bibliografica INT, editorial VARCHAR(100), PRIMARY KEY(id)) ENGINE = INNODB;
CREATE TABLE intervencion (id INT, descripcion_intervencion TEXT NOT NULL, responsables VARCHAR(50), inicio_intervencion DATE, fin_intervencion DATE, sitio_id INT UNSIGNED NOT NULL, tipo_intervencion_id INT UNSIGNED NOT NULL, INDEX sitio_id_idx (sitio_id), INDEX tipo_intervencion_id_idx (tipo_intervencion_id), PRIMARY KEY(id)) ENGINE = INNODB;
CREATE TABLE localidad (id INT UNSIGNED AUTO_INCREMENT, nombre VARCHAR(100) NOT NULL, codigo_postal INT, PRIMARY KEY(id)) ENGINE = INNODB;
CREATE TABLE otro_nombre (id INT UNSIGNED AUTO_INCREMENT, otro_nombre VARCHAR(255) NOT NULL, sitio_id INT UNSIGNED NOT NULL, INDEX sitio_id_idx (sitio_id), PRIMARY KEY(id)) ENGINE = INNODB;
CREATE TABLE reglamentacion (id INT UNSIGNED AUTO_INCREMENT, tipo_reglamentacion VARCHAR(25) NOT NULL, nro_reglamentacion INT NOT NULL, anio_reglamentacion INT NOT NULL, resumen_reglamentacion VARCHAR(100), PRIMARY KEY(id)) ENGINE = INNODB;
CREATE TABLE sitio (id INT UNSIGNED AUTO_INCREMENT, departamento_id INT UNSIGNED NOT NULL, orden_identificacion SMALLINT UNSIGNED NOT NULL, sondeo_identificacion TINYINT(1), latitud_ubicacion CHAR(11), longitud_ubicacion CHAR(11), descripcion_ubicacion TEXT NOT NULL, localidad_id INT UNSIGNED, tipo_de_yacimiento_id INT UNSIGNED, observacion_tipo_yacimiento TEXT, observaciones TEXT, tipo_relevamiento TEXT, identificacion VARCHAR(100) NOT NULL, INDEX tipo_de_yacimiento_id_idx (tipo_de_yacimiento_id), INDEX departamento_id_idx (departamento_id), INDEX localidad_id_idx (localidad_id), PRIMARY KEY(id)) ENGINE = INNODB;
CREATE TABLE sitio_categoria (sitio_id INT UNSIGNED, categoria_id INT UNSIGNED, PRIMARY KEY(sitio_id, categoria_id)) ENGINE = INNODB;
CREATE TABLE sitio_documentacion_inedita (sitio_id INT UNSIGNED, documentacion_inedita_id INT UNSIGNED, PRIMARY KEY(sitio_id, documentacion_inedita_id)) ENGINE = INNODB;
CREATE TABLE sitio_fuente_bibliografica (sitio_id INT UNSIGNED, fuente_bibliografica_id INT UNSIGNED, PRIMARY KEY(sitio_id, fuente_bibliografica_id)) ENGINE = INNODB;
CREATE TABLE sitio_reglamentacion (sitio_id INT UNSIGNED, reglamentacion_id INT UNSIGNED, PRIMARY KEY(sitio_id, reglamentacion_id)) ENGINE = INNODB;
CREATE TABLE tipo_de_yacimiento (id INT UNSIGNED AUTO_INCREMENT, tipo_de_yacimiento VARCHAR(50) NOT NULL, descripcion_tipo_de_yacimiento VARCHAR(50), PRIMARY KEY(id)) ENGINE = INNODB;
CREATE TABLE tipo_intervencion (id INT UNSIGNED AUTO_INCREMENT, tipo_intervencion VARCHAR(50), PRIMARY KEY(id)) ENGINE = INNODB;
CREATE TABLE ubicacion (id INT UNSIGNED AUTO_INCREMENT, latitud CHAR(10), longitud CHAR(10), descripcion_ubicacion TEXT NOT NULL, sitio_id INT UNSIGNED NOT NULL, localidad_id INT UNSIGNED NOT NULL, INDEX sitio_id_idx (sitio_id), INDEX localidad_id_idx (localidad_id), PRIMARY KEY(id)) ENGINE = INNODB;
CREATE TABLE sf_guard_forgot_password (id BIGINT AUTO_INCREMENT, user_id BIGINT NOT NULL, unique_key VARCHAR(255), expires_at DATETIME NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX user_id_idx (user_id), PRIMARY KEY(id)) ENGINE = INNODB;
CREATE TABLE sf_guard_group (id BIGINT AUTO_INCREMENT, name VARCHAR(255) UNIQUE, description TEXT, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, PRIMARY KEY(id)) ENGINE = INNODB;
CREATE TABLE sf_guard_group_permission (group_id BIGINT, permission_id BIGINT, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, PRIMARY KEY(group_id, permission_id)) ENGINE = INNODB;
CREATE TABLE sf_guard_permission (id BIGINT AUTO_INCREMENT, name VARCHAR(255) UNIQUE, description TEXT, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, PRIMARY KEY(id)) ENGINE = INNODB;
CREATE TABLE sf_guard_remember_key (id BIGINT AUTO_INCREMENT, user_id BIGINT, remember_key VARCHAR(32), ip_address VARCHAR(50), created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX user_id_idx (user_id), PRIMARY KEY(id)) ENGINE = INNODB;
CREATE TABLE sf_guard_user (id BIGINT AUTO_INCREMENT, first_name VARCHAR(255), last_name VARCHAR(255), email_address VARCHAR(255) NOT NULL UNIQUE, username VARCHAR(128) NOT NULL UNIQUE, algorithm VARCHAR(128) DEFAULT 'sha1' NOT NULL, salt VARCHAR(128), password VARCHAR(128), is_active TINYINT(1) DEFAULT '1', is_super_admin TINYINT(1) DEFAULT '0', last_login DATETIME, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX is_active_idx_idx (is_active), PRIMARY KEY(id)) ENGINE = INNODB;
CREATE TABLE sf_guard_user_group (user_id BIGINT, group_id BIGINT, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, PRIMARY KEY(user_id, group_id)) ENGINE = INNODB;
CREATE TABLE sf_guard_user_permission (user_id BIGINT, permission_id BIGINT, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, PRIMARY KEY(user_id, permission_id)) ENGINE = INNODB;
ALTER TABLE descripcion_general ADD CONSTRAINT descripcion_general_sitio_id_sitio_id FOREIGN KEY (sitio_id) REFERENCES sitio(id) ON DELETE CASCADE;
ALTER TABLE descripcion_general ADD CONSTRAINT descripcion_general_caracteristica_id_caracteristica_id FOREIGN KEY (caracteristica_id) REFERENCES caracteristica(id);
ALTER TABLE intervencion ADD CONSTRAINT intervencion_tipo_intervencion_id_tipo_intervencion_id FOREIGN KEY (tipo_intervencion_id) REFERENCES tipo_intervencion(id);
ALTER TABLE intervencion ADD CONSTRAINT intervencion_sitio_id_sitio_id FOREIGN KEY (sitio_id) REFERENCES sitio(id) ON DELETE CASCADE;
ALTER TABLE otro_nombre ADD CONSTRAINT otro_nombre_sitio_id_sitio_id FOREIGN KEY (sitio_id) REFERENCES sitio(id) ON DELETE CASCADE;
ALTER TABLE sitio ADD CONSTRAINT sitio_tipo_de_yacimiento_id_tipo_de_yacimiento_id FOREIGN KEY (tipo_de_yacimiento_id) REFERENCES tipo_de_yacimiento(id);
ALTER TABLE sitio ADD CONSTRAINT sitio_localidad_id_localidad_id FOREIGN KEY (localidad_id) REFERENCES localidad(id);
ALTER TABLE sitio ADD CONSTRAINT sitio_departamento_id_departamento_id FOREIGN KEY (departamento_id) REFERENCES departamento(id);
ALTER TABLE sitio_categoria ADD CONSTRAINT sitio_categoria_sitio_id_sitio_id FOREIGN KEY (sitio_id) REFERENCES sitio(id);
ALTER TABLE sitio_categoria ADD CONSTRAINT sitio_categoria_categoria_id_categoria_id FOREIGN KEY (categoria_id) REFERENCES categoria(id);
ALTER TABLE sitio_documentacion_inedita ADD CONSTRAINT sitio_documentacion_inedita_sitio_id_sitio_id FOREIGN KEY (sitio_id) REFERENCES sitio(id);
ALTER TABLE sitio_documentacion_inedita ADD CONSTRAINT sddi FOREIGN KEY (documentacion_inedita_id) REFERENCES documentacion_inedita(id);
ALTER TABLE sitio_fuente_bibliografica ADD CONSTRAINT sitio_fuente_bibliografica_sitio_id_sitio_id FOREIGN KEY (sitio_id) REFERENCES sitio(id);
ALTER TABLE sitio_fuente_bibliografica ADD CONSTRAINT sffi FOREIGN KEY (fuente_bibliografica_id) REFERENCES fuente_bibliografica(id);
ALTER TABLE sitio_reglamentacion ADD CONSTRAINT sitio_reglamentacion_sitio_id_sitio_id FOREIGN KEY (sitio_id) REFERENCES sitio(id);
ALTER TABLE sitio_reglamentacion ADD CONSTRAINT sitio_reglamentacion_reglamentacion_id_reglamentacion_id FOREIGN KEY (reglamentacion_id) REFERENCES reglamentacion(id);
ALTER TABLE ubicacion ADD CONSTRAINT ubicacion_sitio_id_sitio_id FOREIGN KEY (sitio_id) REFERENCES sitio(id);
ALTER TABLE ubicacion ADD CONSTRAINT ubicacion_localidad_id_localidad_id FOREIGN KEY (localidad_id) REFERENCES localidad(id);
ALTER TABLE sf_guard_forgot_password ADD CONSTRAINT sf_guard_forgot_password_user_id_sf_guard_user_id FOREIGN KEY (user_id) REFERENCES sf_guard_user(id) ON DELETE CASCADE;
ALTER TABLE sf_guard_group_permission ADD CONSTRAINT sf_guard_group_permission_permission_id_sf_guard_permission_id FOREIGN KEY (permission_id) REFERENCES sf_guard_permission(id) ON DELETE CASCADE;
ALTER TABLE sf_guard_group_permission ADD CONSTRAINT sf_guard_group_permission_group_id_sf_guard_group_id FOREIGN KEY (group_id) REFERENCES sf_guard_group(id) ON DELETE CASCADE;
ALTER TABLE sf_guard_remember_key ADD CONSTRAINT sf_guard_remember_key_user_id_sf_guard_user_id FOREIGN KEY (user_id) REFERENCES sf_guard_user(id) ON DELETE CASCADE;
ALTER TABLE sf_guard_user_group ADD CONSTRAINT sf_guard_user_group_user_id_sf_guard_user_id FOREIGN KEY (user_id) REFERENCES sf_guard_user(id) ON DELETE CASCADE;
ALTER TABLE sf_guard_user_group ADD CONSTRAINT sf_guard_user_group_group_id_sf_guard_group_id FOREIGN KEY (group_id) REFERENCES sf_guard_group(id) ON DELETE CASCADE;
ALTER TABLE sf_guard_user_permission ADD CONSTRAINT sf_guard_user_permission_user_id_sf_guard_user_id FOREIGN KEY (user_id) REFERENCES sf_guard_user(id) ON DELETE CASCADE;
ALTER TABLE sf_guard_user_permission ADD CONSTRAINT sf_guard_user_permission_permission_id_sf_guard_permission_id FOREIGN KEY (permission_id) REFERENCES sf_guard_permission(id) ON DELETE CASCADE;
