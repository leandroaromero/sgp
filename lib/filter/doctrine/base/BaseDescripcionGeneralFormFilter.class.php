<?php

/**
 * DescripcionGeneral filter form base class.
 *
 * @package    sgp
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseDescripcionGeneralFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'texto_descripcion' => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'sitio_id'          => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Sitio'), 'add_empty' => true)),
      'caracteristica_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Caracteristica'), 'add_empty' => true)),
    ));

    $this->setValidators(array(
      'texto_descripcion' => new sfValidatorPass(array('required' => false)),
      'sitio_id'          => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Sitio'), 'column' => 'id')),
      'caracteristica_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Caracteristica'), 'column' => 'id')),
    ));

    $this->widgetSchema->setNameFormat('descripcion_general_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'DescripcionGeneral';
  }

  public function getFields()
  {
    return array(
      'id'                => 'Number',
      'texto_descripcion' => 'Text',
      'sitio_id'          => 'ForeignKey',
      'caracteristica_id' => 'ForeignKey',
    );
  }
}
