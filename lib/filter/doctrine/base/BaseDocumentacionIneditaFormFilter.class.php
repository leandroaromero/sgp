<?php

/**
 * DocumentacionInedita filter form base class.
 *
 * @package    sgp
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseDocumentacionIneditaFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'nombre'     => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'anio'       => new sfWidgetFormFilterInput(),
      'lugar'      => new sfWidgetFormFilterInput(),
      'sitio_list' => new sfWidgetFormDoctrineChoice(array('multiple' => true, 'model' => 'Sitio')),
    ));

    $this->setValidators(array(
      'nombre'     => new sfValidatorPass(array('required' => false)),
      'anio'       => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'lugar'      => new sfValidatorPass(array('required' => false)),
      'sitio_list' => new sfValidatorDoctrineChoice(array('multiple' => true, 'model' => 'Sitio', 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('documentacion_inedita_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function addSitioListColumnQuery(Doctrine_Query $query, $field, $values)
  {
    if (!is_array($values))
    {
      $values = array($values);
    }

    if (!count($values))
    {
      return;
    }

    $query
      ->leftJoin($query->getRootAlias().'.SitioDocumentacionInedita SitioDocumentacionInedita')
      ->andWhereIn('SitioDocumentacionInedita.sitio_id', $values)
    ;
  }

  public function getModelName()
  {
    return 'DocumentacionInedita';
  }

  public function getFields()
  {
    return array(
      'id'         => 'Number',
      'nombre'     => 'Text',
      'anio'       => 'Number',
      'lugar'      => 'Text',
      'sitio_list' => 'ManyKey',
    );
  }
}
