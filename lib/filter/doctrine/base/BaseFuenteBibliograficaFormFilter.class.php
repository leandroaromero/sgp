<?php

/**
 * FuenteBibliografica filter form base class.
 *
 * @package    sgp
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseFuenteBibliograficaFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'nombre'                     => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'autor_fuente_bibliografica' => new sfWidgetFormFilterInput(),
      'anio_fuente_bibliografica'  => new sfWidgetFormFilterInput(),
      'editorial'                  => new sfWidgetFormFilterInput(),
      'sitio_list'                 => new sfWidgetFormDoctrineChoice(array('multiple' => true, 'model' => 'Sitio')),
    ));

    $this->setValidators(array(
      'nombre'                     => new sfValidatorPass(array('required' => false)),
      'autor_fuente_bibliografica' => new sfValidatorPass(array('required' => false)),
      'anio_fuente_bibliografica'  => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'editorial'                  => new sfValidatorPass(array('required' => false)),
      'sitio_list'                 => new sfValidatorDoctrineChoice(array('multiple' => true, 'model' => 'Sitio', 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('fuente_bibliografica_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function addSitioListColumnQuery(Doctrine_Query $query, $field, $values)
  {
    if (!is_array($values))
    {
      $values = array($values);
    }

    if (!count($values))
    {
      return;
    }

    $query
      ->leftJoin($query->getRootAlias().'.SitioFuenteBibliografica SitioFuenteBibliografica')
      ->andWhereIn('SitioFuenteBibliografica.sitio_id', $values)
    ;
  }

  public function getModelName()
  {
    return 'FuenteBibliografica';
  }

  public function getFields()
  {
    return array(
      'id'                         => 'Number',
      'nombre'                     => 'Text',
      'autor_fuente_bibliografica' => 'Text',
      'anio_fuente_bibliografica'  => 'Number',
      'editorial'                  => 'Text',
      'sitio_list'                 => 'ManyKey',
    );
  }
}
