<?php

/**
 * Institution filter form base class.
 *
 * @package    sgp
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseInstitutionFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'name'      => new sfWidgetFormFilterInput(),
      'subtitle'  => new sfWidgetFormFilterInput(),
      'summary'   => new sfWidgetFormFilterInput(),
      'holder'    => new sfWidgetFormFilterInput(),
      'phone'     => new sfWidgetFormFilterInput(),
      'address'   => new sfWidgetFormFilterInput(),
      'email'     => new sfWidgetFormFilterInput(),
      'facebook'  => new sfWidgetFormFilterInput(),
      'is_active' => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
      'file'      => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'name'      => new sfValidatorPass(array('required' => false)),
      'subtitle'  => new sfValidatorPass(array('required' => false)),
      'summary'   => new sfValidatorPass(array('required' => false)),
      'holder'    => new sfValidatorPass(array('required' => false)),
      'phone'     => new sfValidatorPass(array('required' => false)),
      'address'   => new sfValidatorPass(array('required' => false)),
      'email'     => new sfValidatorPass(array('required' => false)),
      'facebook'  => new sfValidatorPass(array('required' => false)),
      'is_active' => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'file'      => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('institution_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Institution';
  }

  public function getFields()
  {
    return array(
      'id'        => 'Number',
      'name'      => 'Text',
      'subtitle'  => 'Text',
      'summary'   => 'Text',
      'holder'    => 'Text',
      'phone'     => 'Text',
      'address'   => 'Text',
      'email'     => 'Text',
      'facebook'  => 'Text',
      'is_active' => 'Boolean',
      'file'      => 'Text',
    );
  }
}
