<?php

/**
 * Intervencion filter form base class.
 *
 * @package    sgp
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseIntervencionFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'descripcion_intervencion' => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'responsables'             => new sfWidgetFormFilterInput(),
      'inicio_intervencion'      => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
      'fin_intervencion'         => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
      'sitio_id'                 => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Sitio'), 'add_empty' => true)),
      'tipo_intervencion_id'     => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('TipoIntervencion'), 'add_empty' => true)),
    ));

    $this->setValidators(array(
      'descripcion_intervencion' => new sfValidatorPass(array('required' => false)),
      'responsables'             => new sfValidatorPass(array('required' => false)),
      'inicio_intervencion'      => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDateTime(array('required' => false)))),
      'fin_intervencion'         => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDateTime(array('required' => false)))),
      'sitio_id'                 => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Sitio'), 'column' => 'id')),
      'tipo_intervencion_id'     => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('TipoIntervencion'), 'column' => 'id')),
    ));

    $this->widgetSchema->setNameFormat('intervencion_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Intervencion';
  }

  public function getFields()
  {
    return array(
      'id'                       => 'Number',
      'descripcion_intervencion' => 'Text',
      'responsables'             => 'Text',
      'inicio_intervencion'      => 'Date',
      'fin_intervencion'         => 'Date',
      'sitio_id'                 => 'ForeignKey',
      'tipo_intervencion_id'     => 'ForeignKey',
    );
  }
}
