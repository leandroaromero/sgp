<?php

/**
 * OtroNombre filter form base class.
 *
 * @package    sgp
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseOtroNombreFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'otro_nombre' => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'sitio_id'    => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Sitio'), 'add_empty' => true)),
      'periodo'     => new sfWidgetFormFilterInput(),
      'uso'         => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'otro_nombre' => new sfValidatorPass(array('required' => false)),
      'sitio_id'    => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Sitio'), 'column' => 'id')),
      'periodo'     => new sfValidatorPass(array('required' => false)),
      'uso'         => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('otro_nombre_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'OtroNombre';
  }

  public function getFields()
  {
    return array(
      'id'          => 'Number',
      'otro_nombre' => 'Text',
      'sitio_id'    => 'ForeignKey',
      'periodo'     => 'Text',
      'uso'         => 'Text',
    );
  }
}
