<?php

/**
 * Reglamentacion filter form base class.
 *
 * @package    sgp
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseReglamentacionFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'tipo_reglamentacion'    => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'nro_reglamentacion'     => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'anio_reglamentacion'    => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'resumen_reglamentacion' => new sfWidgetFormFilterInput(),
      'jurisdiccion'           => new sfWidgetFormFilterInput(),
      'enlace_reglamentacion'  => new sfWidgetFormFilterInput(),
      'sitio_list'             => new sfWidgetFormDoctrineChoice(array('multiple' => true, 'model' => 'Sitio')),
    ));

    $this->setValidators(array(
      'tipo_reglamentacion'    => new sfValidatorPass(array('required' => false)),
      'nro_reglamentacion'     => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'anio_reglamentacion'    => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'resumen_reglamentacion' => new sfValidatorPass(array('required' => false)),
      'jurisdiccion'           => new sfValidatorPass(array('required' => false)),
      'enlace_reglamentacion'  => new sfValidatorPass(array('required' => false)),
      'sitio_list'             => new sfValidatorDoctrineChoice(array('multiple' => true, 'model' => 'Sitio', 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('reglamentacion_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function addSitioListColumnQuery(Doctrine_Query $query, $field, $values)
  {
    if (!is_array($values))
    {
      $values = array($values);
    }

    if (!count($values))
    {
      return;
    }

    $query
      ->leftJoin($query->getRootAlias().'.SitioReglamentacion SitioReglamentacion')
      ->andWhereIn('SitioReglamentacion.sitio_id', $values)
    ;
  }

  public function getModelName()
  {
    return 'Reglamentacion';
  }

  public function getFields()
  {
    return array(
      'id'                     => 'Number',
      'tipo_reglamentacion'    => 'Text',
      'nro_reglamentacion'     => 'Number',
      'anio_reglamentacion'    => 'Number',
      'resumen_reglamentacion' => 'Text',
      'jurisdiccion'           => 'Text',
      'enlace_reglamentacion'  => 'Text',
      'sitio_list'             => 'ManyKey',
    );
  }
}
