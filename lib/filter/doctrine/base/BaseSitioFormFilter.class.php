<?php

/**
 * Sitio filter form base class.
 *
 * @package    sgp
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseSitioFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'departamento_id'             => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Departamento'), 'add_empty' => true)),
      'orden_identificacion'        => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'sondeo_identificacion'       => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
      'latitud_ubicacion'           => new sfWidgetFormFilterInput(),
      'longitud_ubicacion'          => new sfWidgetFormFilterInput(),
      'texto_ubicacion'             => new sfWidgetFormFilterInput(),
      'descripcion_ubicacion'       => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'localidad_id'                => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Localidad'), 'add_empty' => true)),
      'tipo_de_yacimiento_id'       => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('TipoDeYacimiento'), 'add_empty' => true)),
      'observacion_tipo_yacimiento' => new sfWidgetFormFilterInput(),
      'observaciones'               => new sfWidgetFormFilterInput(),
      'tipo_relevamiento'           => new sfWidgetFormFilterInput(),
      'identificacion'              => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'epoca_construccion'          => new sfWidgetFormFilterInput(),
      'enlace'                      => new sfWidgetFormFilterInput(),
      'created_by'                  => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Creator'), 'add_empty' => true)),
      'updated_by'                  => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Updator'), 'add_empty' => true)),
      'created_at'                  => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'updated_at'                  => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'categoria_list'              => new sfWidgetFormDoctrineChoice(array('multiple' => true, 'model' => 'Categoria')),
      'documentacion_inedita_list'  => new sfWidgetFormDoctrineChoice(array('multiple' => true, 'model' => 'DocumentacionInedita')),
      'fuente_bibliografica_list'   => new sfWidgetFormDoctrineChoice(array('multiple' => true, 'model' => 'FuenteBibliografica')),
      'reglamentacion_list'         => new sfWidgetFormDoctrineChoice(array('multiple' => true, 'model' => 'Reglamentacion')),
    ));

    $this->setValidators(array(
      'departamento_id'             => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Departamento'), 'column' => 'id')),
      'orden_identificacion'        => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'sondeo_identificacion'       => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'latitud_ubicacion'           => new sfValidatorPass(array('required' => false)),
      'longitud_ubicacion'          => new sfValidatorPass(array('required' => false)),
      'texto_ubicacion'             => new sfValidatorPass(array('required' => false)),
      'descripcion_ubicacion'       => new sfValidatorPass(array('required' => false)),
      'localidad_id'                => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Localidad'), 'column' => 'id')),
      'tipo_de_yacimiento_id'       => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('TipoDeYacimiento'), 'column' => 'id')),
      'observacion_tipo_yacimiento' => new sfValidatorPass(array('required' => false)),
      'observaciones'               => new sfValidatorPass(array('required' => false)),
      'tipo_relevamiento'           => new sfValidatorPass(array('required' => false)),
      'identificacion'              => new sfValidatorPass(array('required' => false)),
      'epoca_construccion'          => new sfValidatorPass(array('required' => false)),
      'enlace'                      => new sfValidatorPass(array('required' => false)),
      'created_by'                  => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Creator'), 'column' => 'id')),
      'updated_by'                  => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Updator'), 'column' => 'id')),
      'created_at'                  => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'                  => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'categoria_list'              => new sfValidatorDoctrineChoice(array('multiple' => true, 'model' => 'Categoria', 'required' => false)),
      'documentacion_inedita_list'  => new sfValidatorDoctrineChoice(array('multiple' => true, 'model' => 'DocumentacionInedita', 'required' => false)),
      'fuente_bibliografica_list'   => new sfValidatorDoctrineChoice(array('multiple' => true, 'model' => 'FuenteBibliografica', 'required' => false)),
      'reglamentacion_list'         => new sfValidatorDoctrineChoice(array('multiple' => true, 'model' => 'Reglamentacion', 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('sitio_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function addCategoriaListColumnQuery(Doctrine_Query $query, $field, $values)
  {
    if (!is_array($values))
    {
      $values = array($values);
    }

    if (!count($values))
    {
      return;
    }

    $query
      ->leftJoin($query->getRootAlias().'.SitioCategoria SitioCategoria')
      ->andWhereIn('SitioCategoria.categoria_id', $values)
    ;
  }

  public function addDocumentacionIneditaListColumnQuery(Doctrine_Query $query, $field, $values)
  {
    if (!is_array($values))
    {
      $values = array($values);
    }

    if (!count($values))
    {
      return;
    }

    $query
      ->leftJoin($query->getRootAlias().'.SitioDocumentacionInedita SitioDocumentacionInedita')
      ->andWhereIn('SitioDocumentacionInedita.documentacion_inedita_id', $values)
    ;
  }

  public function addFuenteBibliograficaListColumnQuery(Doctrine_Query $query, $field, $values)
  {
    if (!is_array($values))
    {
      $values = array($values);
    }

    if (!count($values))
    {
      return;
    }

    $query
      ->leftJoin($query->getRootAlias().'.SitioFuenteBibliografica SitioFuenteBibliografica')
      ->andWhereIn('SitioFuenteBibliografica.fuente_bibliografica_id', $values)
    ;
  }

  public function addReglamentacionListColumnQuery(Doctrine_Query $query, $field, $values)
  {
    if (!is_array($values))
    {
      $values = array($values);
    }

    if (!count($values))
    {
      return;
    }

    $query
      ->leftJoin($query->getRootAlias().'.SitioReglamentacion SitioReglamentacion')
      ->andWhereIn('SitioReglamentacion.reglamentacion_id', $values)
    ;
  }

  public function getModelName()
  {
    return 'Sitio';
  }

  public function getFields()
  {
    return array(
      'id'                          => 'Number',
      'departamento_id'             => 'ForeignKey',
      'orden_identificacion'        => 'Number',
      'sondeo_identificacion'       => 'Boolean',
      'latitud_ubicacion'           => 'Text',
      'longitud_ubicacion'          => 'Text',
      'texto_ubicacion'             => 'Text',
      'descripcion_ubicacion'       => 'Text',
      'localidad_id'                => 'ForeignKey',
      'tipo_de_yacimiento_id'       => 'ForeignKey',
      'observacion_tipo_yacimiento' => 'Text',
      'observaciones'               => 'Text',
      'tipo_relevamiento'           => 'Text',
      'identificacion'              => 'Text',
      'epoca_construccion'          => 'Text',
      'enlace'                      => 'Text',
      'created_by'                  => 'ForeignKey',
      'updated_by'                  => 'ForeignKey',
      'created_at'                  => 'Date',
      'updated_at'                  => 'Date',
      'categoria_list'              => 'ManyKey',
      'documentacion_inedita_list'  => 'ManyKey',
      'fuente_bibliografica_list'   => 'ManyKey',
      'reglamentacion_list'         => 'ManyKey',
    );
  }
}
