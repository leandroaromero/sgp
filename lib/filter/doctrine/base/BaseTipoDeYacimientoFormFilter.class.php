<?php

/**
 * TipoDeYacimiento filter form base class.
 *
 * @package    sgp
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseTipoDeYacimientoFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'tipo_de_yacimiento'             => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'descripcion_tipo_de_yacimiento' => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'tipo_de_yacimiento'             => new sfValidatorPass(array('required' => false)),
      'descripcion_tipo_de_yacimiento' => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('tipo_de_yacimiento_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'TipoDeYacimiento';
  }

  public function getFields()
  {
    return array(
      'id'                             => 'Number',
      'tipo_de_yacimiento'             => 'Text',
      'descripcion_tipo_de_yacimiento' => 'Text',
    );
  }
}
