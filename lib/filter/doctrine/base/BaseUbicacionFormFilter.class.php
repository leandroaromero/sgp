<?php

/**
 * Ubicacion filter form base class.
 *
 * @package    sgp
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseUbicacionFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'latitud'               => new sfWidgetFormFilterInput(),
      'longitud'              => new sfWidgetFormFilterInput(),
      'descripcion_ubicacion' => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'sitio_id'              => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Sitio'), 'add_empty' => true)),
      'localidad_id'          => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Localidad'), 'add_empty' => true)),
    ));

    $this->setValidators(array(
      'latitud'               => new sfValidatorPass(array('required' => false)),
      'longitud'              => new sfValidatorPass(array('required' => false)),
      'descripcion_ubicacion' => new sfValidatorPass(array('required' => false)),
      'sitio_id'              => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Sitio'), 'column' => 'id')),
      'localidad_id'          => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Localidad'), 'column' => 'id')),
    ));

    $this->widgetSchema->setNameFormat('ubicacion_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Ubicacion';
  }

  public function getFields()
  {
    return array(
      'id'                    => 'Number',
      'latitud'               => 'Text',
      'longitud'              => 'Text',
      'descripcion_ubicacion' => 'Text',
      'sitio_id'              => 'ForeignKey',
      'localidad_id'          => 'ForeignKey',
    );
  }
}
