<?php

/**
 * DescripcionGeneral form.
 *
 * @package    sgp
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class DescripcionGeneralForm extends BaseDescripcionGeneralForm
{
  public function configure()
  {
      unset($this['sitio_id']);
       if ($this->object->exists())
            {
              $this->widgetSchema['delete'] = new sfWidgetFormInputCheckbox();
              $this->validatorSchema['delete'] = new sfValidatorPass();
            }
            
      $this->getWidgetSchema()->moveField('texto_descripcion', sfWidgetFormSchema::AFTER, 'caracteristica_id');
      $this->widgetSchema['caracteristica_id']->setOption('table_method', 'getOrderNombre');
  }
}
