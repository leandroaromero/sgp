<?php

/**
 * Intervencion form.
 *
 * @package    sgp
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class IntervencionForm extends BaseIntervencionForm
{
  public function configure()
  {
   //   unset($this['sitio_id']);
       if ($this->object->exists())
            {
              $this->widgetSchema['delete'] = new sfWidgetFormInputCheckbox();
              $this->validatorSchema['delete'] = new sfValidatorPass();
            }
      $rango = range(1800, 2020);
          $arreglo_rango = array_combine($rango, $rango);
          $this->widgetSchema['inicio_intervencion'] = new sfWidgetFormJQueryDate(array(
           'label' => 'Fecha *',
           'image'  => '/sgp/images/calendar_icon.gif',
           'culture' => 'es',
           'config' => "{firstDay: 1, dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
            monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
             'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
            buttonText: ['Calendario']}",
           'date_widget' => new sfWidgetFormDate(array(
               'format' => '%day%/%month%/%year%',
               'years' => $arreglo_rango,
               ))
            ));      
       $this->widgetSchema['fin_intervencion'] = new sfWidgetFormJQueryDate(array(
           'label' => 'Fecha *',
           'image'  => '/sgp/images/calendar_icon.gif',
           'culture' => 'es',
           'config' => "{firstDay: 1, dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
            monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
             'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
            buttonText: ['Calendario']}",
           'date_widget' => new sfWidgetFormDate(array(
               'format' => '%day%/%month%/%year%',
               'years' => $arreglo_rango,
               ))
            ));         
  }
}
