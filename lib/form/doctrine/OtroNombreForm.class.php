<?php

/**
 * OtroNombre form.
 *
 * @package    sgp
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class OtroNombreForm extends BaseOtroNombreForm
{
  public function configure()
  {
         unset($this['sitio_id']);
         if ($this->object->exists())
            {
              $this->widgetSchema['delete'] = new sfWidgetFormInputCheckbox();
              $this->validatorSchema['delete'] = new sfValidatorPass();
            }
  }
}
