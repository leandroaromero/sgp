<?php

/**
 * DescripcionGeneral form base class.
 *
 * @method DescripcionGeneral getObject() Returns the current form's model object
 *
 * @package    sgp
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseDescripcionGeneralForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                => new sfWidgetFormInputHidden(),
      'texto_descripcion' => new sfWidgetFormInputText(),
      'sitio_id'          => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Sitio'), 'add_empty' => false)),
      'caracteristica_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Caracteristica'), 'add_empty' => false)),
    ));

    $this->setValidators(array(
      'id'                => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'texto_descripcion' => new sfValidatorString(array('max_length' => 255)),
      'sitio_id'          => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Sitio'))),
      'caracteristica_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Caracteristica'))),
    ));

    $this->widgetSchema->setNameFormat('descripcion_general[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'DescripcionGeneral';
  }

}
