<?php

/**
 * FuenteBibliografica form base class.
 *
 * @method FuenteBibliografica getObject() Returns the current form's model object
 *
 * @package    sgp
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseFuenteBibliograficaForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                         => new sfWidgetFormInputHidden(),
      'nombre'                     => new sfWidgetFormInputText(),
      'autor_fuente_bibliografica' => new sfWidgetFormInputText(),
      'anio_fuente_bibliografica'  => new sfWidgetFormInputText(),
      'editorial'                  => new sfWidgetFormInputText(),
      'sitio_list'                 => new sfWidgetFormDoctrineChoice(array('multiple' => true, 'model' => 'Sitio')),
    ));

    $this->setValidators(array(
      'id'                         => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'nombre'                     => new sfValidatorString(array('max_length' => 100)),
      'autor_fuente_bibliografica' => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'anio_fuente_bibliografica'  => new sfValidatorInteger(array('required' => false)),
      'editorial'                  => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'sitio_list'                 => new sfValidatorDoctrineChoice(array('multiple' => true, 'model' => 'Sitio', 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('fuente_bibliografica[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'FuenteBibliografica';
  }

  public function updateDefaultsFromObject()
  {
    parent::updateDefaultsFromObject();

    if (isset($this->widgetSchema['sitio_list']))
    {
      $this->setDefault('sitio_list', $this->object->Sitio->getPrimaryKeys());
    }

  }

  protected function doSave($con = null)
  {
    $this->saveSitioList($con);

    parent::doSave($con);
  }

  public function saveSitioList($con = null)
  {
    if (!$this->isValid())
    {
      throw $this->getErrorSchema();
    }

    if (!isset($this->widgetSchema['sitio_list']))
    {
      // somebody has unset this widget
      return;
    }

    if (null === $con)
    {
      $con = $this->getConnection();
    }

    $existing = $this->object->Sitio->getPrimaryKeys();
    $values = $this->getValue('sitio_list');
    if (!is_array($values))
    {
      $values = array();
    }

    $unlink = array_diff($existing, $values);
    if (count($unlink))
    {
      $this->object->unlink('Sitio', array_values($unlink));
    }

    $link = array_diff($values, $existing);
    if (count($link))
    {
      $this->object->link('Sitio', array_values($link));
    }
  }

}
