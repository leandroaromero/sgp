<?php

/**
 * Intervencion form base class.
 *
 * @method Intervencion getObject() Returns the current form's model object
 *
 * @package    sgp
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseIntervencionForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                       => new sfWidgetFormInputHidden(),
      'descripcion_intervencion' => new sfWidgetFormTextarea(),
      'responsables'             => new sfWidgetFormInputText(),
      'inicio_intervencion'      => new sfWidgetFormDate(),
      'fin_intervencion'         => new sfWidgetFormDate(),
      'sitio_id'                 => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Sitio'), 'add_empty' => false)),
      'tipo_intervencion_id'     => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('TipoIntervencion'), 'add_empty' => false)),
    ));

    $this->setValidators(array(
      'id'                       => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'descripcion_intervencion' => new sfValidatorString(),
      'responsables'             => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'inicio_intervencion'      => new sfValidatorDate(array('required' => false)),
      'fin_intervencion'         => new sfValidatorDate(array('required' => false)),
      'sitio_id'                 => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Sitio'))),
      'tipo_intervencion_id'     => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('TipoIntervencion'))),
    ));

    $this->widgetSchema->setNameFormat('intervencion[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Intervencion';
  }

}
