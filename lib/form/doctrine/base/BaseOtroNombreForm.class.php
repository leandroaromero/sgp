<?php

/**
 * OtroNombre form base class.
 *
 * @method OtroNombre getObject() Returns the current form's model object
 *
 * @package    sgp
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseOtroNombreForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'          => new sfWidgetFormInputHidden(),
      'otro_nombre' => new sfWidgetFormInputText(),
      'sitio_id'    => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Sitio'), 'add_empty' => false)),
      'periodo'     => new sfWidgetFormInputText(),
      'uso'         => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'          => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'otro_nombre' => new sfValidatorString(array('max_length' => 255)),
      'sitio_id'    => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Sitio'))),
      'periodo'     => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'uso'         => new sfValidatorString(array('max_length' => 50, 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('otro_nombre[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'OtroNombre';
  }

}
