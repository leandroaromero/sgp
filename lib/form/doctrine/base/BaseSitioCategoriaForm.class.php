<?php

/**
 * SitioCategoria form base class.
 *
 * @method SitioCategoria getObject() Returns the current form's model object
 *
 * @package    sgp
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseSitioCategoriaForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'sitio_id'     => new sfWidgetFormInputHidden(),
      'categoria_id' => new sfWidgetFormInputHidden(),
    ));

    $this->setValidators(array(
      'sitio_id'     => new sfValidatorChoice(array('choices' => array($this->getObject()->get('sitio_id')), 'empty_value' => $this->getObject()->get('sitio_id'), 'required' => false)),
      'categoria_id' => new sfValidatorChoice(array('choices' => array($this->getObject()->get('categoria_id')), 'empty_value' => $this->getObject()->get('categoria_id'), 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('sitio_categoria[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'SitioCategoria';
  }

}
