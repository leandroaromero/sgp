<?php

/**
 * SitioDocumentacionInedita form base class.
 *
 * @method SitioDocumentacionInedita getObject() Returns the current form's model object
 *
 * @package    sgp
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseSitioDocumentacionIneditaForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'sitio_id'                 => new sfWidgetFormInputHidden(),
      'documentacion_inedita_id' => new sfWidgetFormInputHidden(),
    ));

    $this->setValidators(array(
      'sitio_id'                 => new sfValidatorChoice(array('choices' => array($this->getObject()->get('sitio_id')), 'empty_value' => $this->getObject()->get('sitio_id'), 'required' => false)),
      'documentacion_inedita_id' => new sfValidatorChoice(array('choices' => array($this->getObject()->get('documentacion_inedita_id')), 'empty_value' => $this->getObject()->get('documentacion_inedita_id'), 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('sitio_documentacion_inedita[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'SitioDocumentacionInedita';
  }

}
