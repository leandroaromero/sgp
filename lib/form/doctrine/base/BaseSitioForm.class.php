<?php

/**
 * Sitio form base class.
 *
 * @method Sitio getObject() Returns the current form's model object
 *
 * @package    sgp
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseSitioForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                          => new sfWidgetFormInputHidden(),
      'departamento_id'             => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Departamento'), 'add_empty' => false)),
      'orden_identificacion'        => new sfWidgetFormInputText(),
      'sondeo_identificacion'       => new sfWidgetFormInputCheckbox(),
      'latitud_ubicacion'           => new sfWidgetFormInputText(),
      'longitud_ubicacion'          => new sfWidgetFormInputText(),
      'texto_ubicacion'             => new sfWidgetFormInputText(),
      'descripcion_ubicacion'       => new sfWidgetFormTextarea(),
      'localidad_id'                => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Localidad'), 'add_empty' => true)),
      'tipo_de_yacimiento_id'       => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('TipoDeYacimiento'), 'add_empty' => true)),
      'observacion_tipo_yacimiento' => new sfWidgetFormTextarea(),
      'observaciones'               => new sfWidgetFormTextarea(),
      'tipo_relevamiento'           => new sfWidgetFormTextarea(),
      'identificacion'              => new sfWidgetFormInputText(),
      'epoca_construccion'          => new sfWidgetFormInputText(),
      'enlace'                      => new sfWidgetFormTextarea(),
      'created_by'                  => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Creator'), 'add_empty' => true)),
      'updated_by'                  => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Updator'), 'add_empty' => true)),
      'created_at'                  => new sfWidgetFormDateTime(),
      'updated_at'                  => new sfWidgetFormDateTime(),
      'categoria_list'              => new sfWidgetFormDoctrineChoice(array('multiple' => true, 'model' => 'Categoria')),
      'documentacion_inedita_list'  => new sfWidgetFormDoctrineChoice(array('multiple' => true, 'model' => 'DocumentacionInedita')),
      'fuente_bibliografica_list'   => new sfWidgetFormDoctrineChoice(array('multiple' => true, 'model' => 'FuenteBibliografica')),
      'reglamentacion_list'         => new sfWidgetFormDoctrineChoice(array('multiple' => true, 'model' => 'Reglamentacion')),
    ));

    $this->setValidators(array(
      'id'                          => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'departamento_id'             => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Departamento'))),
      'orden_identificacion'        => new sfValidatorInteger(),
      'sondeo_identificacion'       => new sfValidatorBoolean(array('required' => false)),
      'latitud_ubicacion'           => new sfValidatorString(array('max_length' => 11, 'required' => false)),
      'longitud_ubicacion'          => new sfValidatorString(array('max_length' => 11, 'required' => false)),
      'texto_ubicacion'             => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'descripcion_ubicacion'       => new sfValidatorString(),
      'localidad_id'                => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Localidad'), 'required' => false)),
      'tipo_de_yacimiento_id'       => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('TipoDeYacimiento'), 'required' => false)),
      'observacion_tipo_yacimiento' => new sfValidatorString(array('required' => false)),
      'observaciones'               => new sfValidatorString(array('required' => false)),
      'tipo_relevamiento'           => new sfValidatorString(array('required' => false)),
      'identificacion'              => new sfValidatorString(array('max_length' => 100)),
      'epoca_construccion'          => new sfValidatorString(array('max_length' => 59, 'required' => false)),
      'enlace'                      => new sfValidatorString(array('required' => false)),
      'created_by'                  => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Creator'), 'required' => false)),
      'updated_by'                  => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Updator'), 'required' => false)),
      'created_at'                  => new sfValidatorDateTime(),
      'updated_at'                  => new sfValidatorDateTime(),
      'categoria_list'              => new sfValidatorDoctrineChoice(array('multiple' => true, 'model' => 'Categoria', 'required' => false)),
      'documentacion_inedita_list'  => new sfValidatorDoctrineChoice(array('multiple' => true, 'model' => 'DocumentacionInedita', 'required' => false)),
      'fuente_bibliografica_list'   => new sfValidatorDoctrineChoice(array('multiple' => true, 'model' => 'FuenteBibliografica', 'required' => false)),
      'reglamentacion_list'         => new sfValidatorDoctrineChoice(array('multiple' => true, 'model' => 'Reglamentacion', 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('sitio[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Sitio';
  }

  public function updateDefaultsFromObject()
  {
    parent::updateDefaultsFromObject();

    if (isset($this->widgetSchema['categoria_list']))
    {
      $this->setDefault('categoria_list', $this->object->Categoria->getPrimaryKeys());
    }

    if (isset($this->widgetSchema['documentacion_inedita_list']))
    {
      $this->setDefault('documentacion_inedita_list', $this->object->DocumentacionInedita->getPrimaryKeys());
    }

    if (isset($this->widgetSchema['fuente_bibliografica_list']))
    {
      $this->setDefault('fuente_bibliografica_list', $this->object->FuenteBibliografica->getPrimaryKeys());
    }

    if (isset($this->widgetSchema['reglamentacion_list']))
    {
      $this->setDefault('reglamentacion_list', $this->object->Reglamentacion->getPrimaryKeys());
    }

  }

  protected function doSave($con = null)
  {
    $this->saveCategoriaList($con);
    $this->saveDocumentacionIneditaList($con);
    $this->saveFuenteBibliograficaList($con);
    $this->saveReglamentacionList($con);

    parent::doSave($con);
  }

  public function saveCategoriaList($con = null)
  {
    if (!$this->isValid())
    {
      throw $this->getErrorSchema();
    }

    if (!isset($this->widgetSchema['categoria_list']))
    {
      // somebody has unset this widget
      return;
    }

    if (null === $con)
    {
      $con = $this->getConnection();
    }

    $existing = $this->object->Categoria->getPrimaryKeys();
    $values = $this->getValue('categoria_list');
    if (!is_array($values))
    {
      $values = array();
    }

    $unlink = array_diff($existing, $values);
    if (count($unlink))
    {
      $this->object->unlink('Categoria', array_values($unlink));
    }

    $link = array_diff($values, $existing);
    if (count($link))
    {
      $this->object->link('Categoria', array_values($link));
    }
  }

  public function saveDocumentacionIneditaList($con = null)
  {
    if (!$this->isValid())
    {
      throw $this->getErrorSchema();
    }

    if (!isset($this->widgetSchema['documentacion_inedita_list']))
    {
      // somebody has unset this widget
      return;
    }

    if (null === $con)
    {
      $con = $this->getConnection();
    }

    $existing = $this->object->DocumentacionInedita->getPrimaryKeys();
    $values = $this->getValue('documentacion_inedita_list');
    if (!is_array($values))
    {
      $values = array();
    }

    $unlink = array_diff($existing, $values);
    if (count($unlink))
    {
      $this->object->unlink('DocumentacionInedita', array_values($unlink));
    }

    $link = array_diff($values, $existing);
    if (count($link))
    {
      $this->object->link('DocumentacionInedita', array_values($link));
    }
  }

  public function saveFuenteBibliograficaList($con = null)
  {
    if (!$this->isValid())
    {
      throw $this->getErrorSchema();
    }

    if (!isset($this->widgetSchema['fuente_bibliografica_list']))
    {
      // somebody has unset this widget
      return;
    }

    if (null === $con)
    {
      $con = $this->getConnection();
    }

    $existing = $this->object->FuenteBibliografica->getPrimaryKeys();
    $values = $this->getValue('fuente_bibliografica_list');
    if (!is_array($values))
    {
      $values = array();
    }

    $unlink = array_diff($existing, $values);
    if (count($unlink))
    {
      $this->object->unlink('FuenteBibliografica', array_values($unlink));
    }

    $link = array_diff($values, $existing);
    if (count($link))
    {
      $this->object->link('FuenteBibliografica', array_values($link));
    }
  }

  public function saveReglamentacionList($con = null)
  {
    if (!$this->isValid())
    {
      throw $this->getErrorSchema();
    }

    if (!isset($this->widgetSchema['reglamentacion_list']))
    {
      // somebody has unset this widget
      return;
    }

    if (null === $con)
    {
      $con = $this->getConnection();
    }

    $existing = $this->object->Reglamentacion->getPrimaryKeys();
    $values = $this->getValue('reglamentacion_list');
    if (!is_array($values))
    {
      $values = array();
    }

    $unlink = array_diff($existing, $values);
    if (count($unlink))
    {
      $this->object->unlink('Reglamentacion', array_values($unlink));
    }

    $link = array_diff($values, $existing);
    if (count($link))
    {
      $this->object->link('Reglamentacion', array_values($link));
    }
  }

}
