<?php

/**
 * SitioFuenteBibliografica form base class.
 *
 * @method SitioFuenteBibliografica getObject() Returns the current form's model object
 *
 * @package    sgp
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseSitioFuenteBibliograficaForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'sitio_id'                => new sfWidgetFormInputHidden(),
      'fuente_bibliografica_id' => new sfWidgetFormInputHidden(),
    ));

    $this->setValidators(array(
      'sitio_id'                => new sfValidatorChoice(array('choices' => array($this->getObject()->get('sitio_id')), 'empty_value' => $this->getObject()->get('sitio_id'), 'required' => false)),
      'fuente_bibliografica_id' => new sfValidatorChoice(array('choices' => array($this->getObject()->get('fuente_bibliografica_id')), 'empty_value' => $this->getObject()->get('fuente_bibliografica_id'), 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('sitio_fuente_bibliografica[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'SitioFuenteBibliografica';
  }

}
