<?php

/**
 * SitioReglamentacion form base class.
 *
 * @method SitioReglamentacion getObject() Returns the current form's model object
 *
 * @package    sgp
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseSitioReglamentacionForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'sitio_id'          => new sfWidgetFormInputHidden(),
      'reglamentacion_id' => new sfWidgetFormInputHidden(),
    ));

    $this->setValidators(array(
      'sitio_id'          => new sfValidatorChoice(array('choices' => array($this->getObject()->get('sitio_id')), 'empty_value' => $this->getObject()->get('sitio_id'), 'required' => false)),
      'reglamentacion_id' => new sfValidatorChoice(array('choices' => array($this->getObject()->get('reglamentacion_id')), 'empty_value' => $this->getObject()->get('reglamentacion_id'), 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('sitio_reglamentacion[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'SitioReglamentacion';
  }

}
