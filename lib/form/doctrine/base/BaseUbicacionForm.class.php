<?php

/**
 * Ubicacion form base class.
 *
 * @method Ubicacion getObject() Returns the current form's model object
 *
 * @package    sgp
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseUbicacionForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                    => new sfWidgetFormInputHidden(),
      'latitud'               => new sfWidgetFormInputText(),
      'longitud'              => new sfWidgetFormInputText(),
      'descripcion_ubicacion' => new sfWidgetFormTextarea(),
      'sitio_id'              => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Sitio'), 'add_empty' => false)),
      'localidad_id'          => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Localidad'), 'add_empty' => false)),
    ));

    $this->setValidators(array(
      'id'                    => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'latitud'               => new sfValidatorString(array('max_length' => 10, 'required' => false)),
      'longitud'              => new sfValidatorString(array('max_length' => 10, 'required' => false)),
      'descripcion_ubicacion' => new sfValidatorString(),
      'sitio_id'              => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Sitio'))),
      'localidad_id'          => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Localidad'))),
    ));

    $this->widgetSchema->setNameFormat('ubicacion[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Ubicacion';
  }

}
