<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Publico
 *
 * @author leandro
 */
class Publico {
    
  public $index;  
  public $id;
  
  
  //Departamento 	Identificación codificada desc 	Otro nombres 	Descripcion
  public $categorias;
  public $departamento_id;
  public $identificacion;  
  public $otroNombre;
  public $descripcion;  
  public $file;
  public $longitud;
  public $lalitud;


  public $imagen1;
  public $imagenes;



      public function __construct(Sitio $obj, Array $arra, $index = 1 )
      {

            $this->index=$index;
            $this->id= $obj->getId();
            $this->departamento_id= $obj->getDepartamentoId();
            $this->identificacion = $obj->getIdentificacion();
            $this->descripcion = $obj->getDescripcionUbicacion();
            $this->lalitud= $obj->getLatitudUbicacion();
            $this->longitud= $obj->getLongitudUbicacion();
            $this->file = $arra['file'] ;

      }
  

      public function getId(){
            return $this->id;
        }
      public function getIndex(){
            return $this->index;
        }
        
       public function getCategoria(){
           
            if( ($this->categorias == Null) ||($this->categorias == '')){
                  $conn= Doctrine_Manager::getInstance()->getConnection($this->file );
                  foreach ($this->getCategoriaSearch( $conn) as $mater){
                          $this->categorias.=$mater->getNombre().', ';
                   }
              }
            return $this->categorias;
      }  
      public function getNombre(){    
            $name='';
            switch ($this->file){
              case 'archeology':
                  $name='Arqueológicos';
                  break;
              default :
                  $name='Arquitectónicos';
                 break;
            }
             return $name;
      }
      
      public function getIdentificacion(){
          return $this->identificacion;
      }
      public function getDescripcion(){
          return $this->descripcion;
      }
      public function getFile(){
           return $this->file;
      }
      public function getLatitudUbicacion(){
           return $this->lalitud;
      }
      public function getLongitudUbicacion(){
           return $this->longitud;
      }
      
      
      public function getDepartamento(){
          if( $this->id > 0){
              
           $conn= Doctrine_Manager::getInstance()->getConnection($this->file ); 
           $q = Doctrine_Query::create($conn)
                   ->from('Departamento d')
                   ->where('d.id = ?', $this->departamento_id);
           $departamento= $q->fetchOne();
          
           $nombre= $departamento->getNombre();
          }else{
              $nombre='';
          }
          return $nombre;
      }
      public function getOtrosNombres(){
          
            if( ($this->otroNombre == Null) ||($this->otroNombre == '')){
                  $conn= Doctrine_Manager::getInstance()->getConnection($this->file );
                  foreach ($this->getOtrosNombresSearch( $conn) as $mater){
                          $this->otroNombre.=$mater->getOtroNombre().', ';
                   }
              }
              
          return $this->otroNombre;
      }
      

  public function getOtrosNombresSearch($conn){

          $q = Doctrine_Query::create($conn)
                   ->from('OtroNombre o')
                   ->leftJoin('o.Sitio s')     
                   ->Where('s.id = ?', $this->id);
          return $q->execute();
  }
  
  public function getCategoriaSearch($conn){

          $q = Doctrine_Query::create($conn)
                   ->from('Categoria o')
                   ->leftJoin('o.SitioCategoria sc')     
                   ->Where('sc.sitio_id = ?', $this->id);
          return $q->execute();
  }
  

}


?>
