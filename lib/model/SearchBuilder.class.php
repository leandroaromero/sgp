<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


class SearchBuilder 
{
    public $museo;
    public $general;


    public function __construct($dato)
  {   
        
       $this->museo= $dato['museo'];
       
       if(isset($dato['general']))
       $this->general= $dato['general'];
       
       
    }
    public function getBasesDatos(){
       $this->respuesta= array();
       $this->respuestaTotal= array();
       switch ($this->museo){
        case 'archeology':
                $this->respuesta['file'] = 'archeology';
              break;
        case 'architecture':    
                $this->respuesta['file'] = 'architecture';
              break;
        default :
                $this->respuesta['file'] = 'archeology';
                $this->respuestaTotal[]=$this->respuesta;
                $this->respuesta['file'] = 'architecture';
              break;
          
       }
        $this->respuestaTotal[]=$this->respuesta;
        return $this->respuestaTotal;
    }
    
    public function getResultPager(Array $result){
       
        
    }
    public function getInstitutionName(){
    
        $name='';
        switch ($this->museo){
          case 'archeology':
              $name='Sitios Arqueológicos';
              break;
          case 'architecture':
              $name='Sitios Arquitectónicos';
              break;
          default :
              $name='Todos los sitios';
        }
              return $name;

    }
    public function getGeneral(){
        return $this->general;
    }
    
    
    public function getBusquedas(){
        $dato=array();
        $dato['museo']       = $this->getInstitutionName() ;
        $dato['general']      = $this->getGeneral();
        
        return $dato;
    }
} 
?>
