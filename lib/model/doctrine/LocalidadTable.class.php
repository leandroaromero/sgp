<?php

/**
 * LocalidadTable
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class LocalidadTable extends Doctrine_Table
{
    /**
     * Returns an instance of this class.
     *
     * @return object LocalidadTable
     */
    public static function getInstance()
    {
        return Doctrine_Core::getTable('Localidad');
    }
    public static function getOrderNombre(){
        $q = Doctrine_Query::create()
        ->from('Localidad t')
        ->orderBy('t.nombre ASC');

       return $q->execute();
    }    
}