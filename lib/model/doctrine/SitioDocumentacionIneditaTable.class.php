<?php

/**
 * SitioDocumentacionIneditaTable
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class SitioDocumentacionIneditaTable extends Doctrine_Table
{
    /**
     * Returns an instance of this class.
     *
     * @return object SitioDocumentacionIneditaTable
     */
    public static function getInstance()
    {
        return Doctrine_Core::getTable('SitioDocumentacionInedita');
    }
}