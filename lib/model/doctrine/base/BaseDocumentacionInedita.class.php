<?php
// Connection Component Binding
Doctrine_Manager::getInstance()->bindComponent('DocumentacionInedita', sfContext::getInstance()->getUser()->getDataBase()  );

/**
 * BaseDocumentacionInedita
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property integer $id
 * @property string $nombre
 * @property integer $anio
 * @property string $lugar
 * @property Doctrine_Collection $Sitio
 * 
 * @method integer              getId()                        Returns the current record's "id" value
 * @method string               getNombre()                    Returns the current record's "nombre" value
 * @method integer              getAnio()                      Returns the current record's "anio" value
 * @method string               getLugar()                     Returns the current record's "lugar" value
 * @method Doctrine_Collection  getSitioDocumentacionInedita() Returns the current record's "SitioDocumentacionInedita" collection
 * @method Doctrine_Collection  getSitio()                     Returns the current record's "Sitio" collection
 * @method DocumentacionInedita setId()                        Sets the current record's "id" value
 * @method DocumentacionInedita setNombre()                    Sets the current record's "nombre" value
 * @method DocumentacionInedita setAnio()                      Sets the current record's "anio" value
 * @method DocumentacionInedita setLugar()                     Sets the current record's "lugar" value
 * @method DocumentacionInedita setSitioDocumentacionInedita() Sets the current record's "SitioDocumentacionInedita" collection
 * @method DocumentacionInedita setSitio()                     Sets the current record's "Sitio" collectionDocumentacionInedita
 * @property Doctrine_Collection $Sitio
 * 
 * @method integer              getId()                        Returns the current record's "id" value
 * @method string               getNombre()                    Returns the current record's "nombre" value
 * @method integer              getAnio()                      Returns the current record's "anio" value
 * @method string               getLugar()                     Returns the current record's "lugar" value
 * @method Doctrine_Collection  getSitioDocumentacionInedita() Returns the current record's "SitioDocumentacionInedita" collection
 * @method Doctrine_Collection  getSitio()                     Returns the current record's "Sitio" collection
 * @method DocumentacionInedita setId()                        Sets the current record's "id" value
 * @method DocumentacionInedita setNombre()                    Sets the current record's "nombre" value
 * @method DocumentacionInedita setAnio()                      Sets the current record's "anio" value
 * @method DocumentacionInedita setLugar()                     Sets the current record's "lugar" value
 * @method DocumentacionInedita setSitioDocumentacionInedita() Sets the current record's "SitioDocumentacionInedita" collection
 * @method DocumentacionInedita setSitio()                     Sets the current record's "Sitio" collection
 * 
 * @package    sgp
 * @subpackage model
 * @author     Your name here
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BaseDocumentacionInedita extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('documentacion_inedita');
        $this->hasColumn('id', 'integer', 4, array(
             'type' => 'integer',
             'fixed' => 0,
             'unsigned' => true,
             'primary' => true,
             'autoincrement' => true,
             'length' => 4,
             ));
        $this->hasColumn('nombre', 'string', 100, array(
             'type' => 'string',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => true,
             'autoincrement' => false,
             'length' => 100,
             ));
        $this->hasColumn('anio', 'integer', 4, array(
             'type' => 'integer',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => false,
             'autoincrement' => false,
             'length' => 4,
             ));
        $this->hasColumn('lugar', 'string', 100, array(
             'type' => 'string',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => false,
             'autoincrement' => false,
             'length' => 100,
             ));
    }

    public function setUp()
    {
        parent::setUp();
        $this->hasMany('SitioDocumentacionInedita', array(
             'local' => 'id',
             'foreign' => 'documentacion_inedita_id'));

        $this->hasMany('Sitio', array(
             'refClass' => 'SitioDocumentacionInedita',
             'local' => 'documentacion_inedita_id',
             'foreign' => 'sitio_id'));
    }
}