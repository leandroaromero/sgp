<?php
// Connection Component Binding
Doctrine_Manager::getInstance()->bindComponent('Reglamentacion', sfContext::getInstance()->getUser()->getDataBase()  );

/**
 * BaseReglamentacion
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property integer $id
 * @property string $tipo_reglamentacion
 * @property integer $nro_reglamentacion
 * @property integer $anio_reglamentacion
 * @property string $resumen_reglamentacion
 * @property string $jurisdiccion
 * @property string $enlace_reglamentacion
 * @property Doctrine_Collection $Sitio
 * 
 * @method integer             getId()                     Returns the current record's "id" value
 * @method string              getTipoReglamentacion()     Returns the current record's "tipo_reglamentacion" value
 * @method integer             getNroReglamentacion()      Returns the current record's "nro_reglamentacion" value
 * @method integer             getAnioReglamentacion()     Returns the current record's "anio_reglamentacion" value
 * @method string              getResumenReglamentacion()  Returns the current record's "resumen_reglamentacion" value
 * @method string              getJurisdiccion()           Returns the current record's "jurisdiccion" value
 * @method string              getEnlaceReglamentacion()   Returns the current record's "enlace_reglamentacion" value
 * @method Doctrine_Collection getSitioReglamentacion()    Returns the current record's "SitioReglamentacion" collection
 * @method Doctrine_Collection getSitio()                  Returns the current record's "Sitio" collection
 * @method Reglamentacion      setId()                     Sets the current record's "id" value
 * @method Reglamentacion      setTipoReglamentacion()     Sets the current record's "tipo_reglamentacion" value
 * @method Reglamentacion      setNroReglamentacion()      Sets the current record's "nro_reglamentacion" value
 * @method Reglamentacion      setAnioReglamentacion()     Sets the current record's "anio_reglamentacion" value
 * @method Reglamentacion      setResumenReglamentacion()  Sets the current record's "resumen_reglamentacion" value
 * @method Reglamentacion      setJurisdiccion()           Sets the current record's "jurisdiccion" value
 * @method Reglamentacion      setEnlaceReglamentacion()   Sets the current record's "enlace_reglamentacion" value
 * @method Reglamentacion      setSitioReglamentacion()    Sets the current record's "SitioReglamentacion" collection
 * @method Reglamentacion      setSitio()                  Sets the current record's "Sitio" collectionReglamentacion
 * @property Doctrine_Collection $Sitio
 * 
 * @method integer             getId()                     Returns the current record's "id" value
 * @method string              getTipoReglamentacion()     Returns the current record's "tipo_reglamentacion" value
 * @method integer             getNroReglamentacion()      Returns the current record's "nro_reglamentacion" value
 * @method integer             getAnioReglamentacion()     Returns the current record's "anio_reglamentacion" value
 * @method string              getResumenReglamentacion()  Returns the current record's "resumen_reglamentacion" value
 * @method string              getJurisdiccion()           Returns the current record's "jurisdiccion" value
 * @method string              getEnlaceReglamentacion()   Returns the current record's "enlace_reglamentacion" value
 * @method Doctrine_Collection getSitioReglamentacion()    Returns the current record's "SitioReglamentacion" collection
 * @method Doctrine_Collection getSitio()                  Returns the current record's "Sitio" collection
 * @method Reglamentacion      setId()                     Sets the current record's "id" value
 * @method Reglamentacion      setTipoReglamentacion()     Sets the current record's "tipo_reglamentacion" value
 * @method Reglamentacion      setNroReglamentacion()      Sets the current record's "nro_reglamentacion" value
 * @method Reglamentacion      setAnioReglamentacion()     Sets the current record's "anio_reglamentacion" value
 * @method Reglamentacion      setResumenReglamentacion()  Sets the current record's "resumen_reglamentacion" value
 * @method Reglamentacion      setJurisdiccion()           Sets the current record's "jurisdiccion" value
 * @method Reglamentacion      setEnlaceReglamentacion()   Sets the current record's "enlace_reglamentacion" value
 * @method Reglamentacion      setSitioReglamentacion()    Sets the current record's "SitioReglamentacion" collection
 * @method Reglamentacion      setSitio()                  Sets the current record's "Sitio" collection
 * 
 * @package    sgp
 * @subpackage model
 * @author     Your name here
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BaseReglamentacion extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('reglamentacion');
        $this->hasColumn('id', 'integer', 4, array(
             'type' => 'integer',
             'fixed' => 0,
             'unsigned' => true,
             'primary' => true,
             'autoincrement' => true,
             'length' => 4,
             ));
        $this->hasColumn('tipo_reglamentacion', 'string', 25, array(
             'type' => 'string',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => true,
             'autoincrement' => false,
             'length' => 25,
             ));
        $this->hasColumn('nro_reglamentacion', 'integer', 4, array(
             'type' => 'integer',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => true,
             'autoincrement' => false,
             'length' => 4,
             ));
        $this->hasColumn('anio_reglamentacion', 'integer', 4, array(
             'type' => 'integer',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => true,
             'autoincrement' => false,
             'length' => 4,
             ));
        $this->hasColumn('resumen_reglamentacion', 'string', 100, array(
             'type' => 'string',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => false,
             'autoincrement' => false,
             'length' => 100,
             ));
        $this->hasColumn('jurisdiccion', 'string', 50, array(
             'type' => 'string',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => false,
             'autoincrement' => false,
             'length' => 50,
             ));
        $this->hasColumn('enlace_reglamentacion', 'string', 255, array(
             'type' => 'string',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => false,
             'autoincrement' => false,
             'length' => 255,
             ));
    }

    public function setUp()
    {
        parent::setUp();
        $this->hasMany('SitioReglamentacion', array(
             'local' => 'id',
             'foreign' => 'reglamentacion_id'));

        $this->hasMany('Sitio', array(
             'refClass' => 'SitioReglamentacion',
             'local' => 'reglamentacion_id',
             'foreign' => 'sitio_id'));
    }
}