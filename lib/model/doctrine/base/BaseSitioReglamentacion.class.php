<?php
// Connection Component Binding
Doctrine_Manager::getInstance()->bindComponent('SitioReglamentacion', sfContext::getInstance()->getUser()->getDataBase()  );

/**
 * BaseSitioReglamentacion
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property integer $sitio_id
 * @property integer $reglamentacion_id
 * @property Sitio $Sitio
 * @property Reglamentacion $Reglamentacion
 * 
 * @method integer             getSitioId()           Returns the current record's "sitio_id" value
 * @method integer             getReglamentacionId()  Returns the current record's "reglamentacion_id" value
 * @method Sitio               getSitio()             Returns the current record's "Sitio" value
 * @method Reglamentacion      getReglamentacion()    Returns the current record's "Reglamentacion" value
 * @method SitioReglamentacion setSitioId()           Sets the current record's "sitio_id" value
 * @method SitioReglamentacion setReglamentacionId()  Sets the current record's "reglamentacion_id" value
 * @method SitioReglamentacion setSitio()             Sets the current record's "Sitio" value
 * @method SitioReglamentacion setReglamentacion()    Sets the current record's "Reglamentacion" value
 * 
 * @package    sgp
 * @subpackage model
 * @author     Your name here
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BaseSitioReglamentacion extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('sitio_reglamentacion');
        $this->hasColumn('sitio_id', 'integer', 4, array(
             'type' => 'integer',
             'fixed' => 0,
             'unsigned' => true,
             'primary' => true,
             'autoincrement' => false,
             'length' => 4,
             ));
        $this->hasColumn('reglamentacion_id', 'integer', 4, array(
             'type' => 'integer',
             'fixed' => 0,
             'unsigned' => true,
             'primary' => true,
             'autoincrement' => false,
             'length' => 4,
             ));
    }

    public function setUp()
    {
        parent::setUp();
        $this->hasOne('Sitio', array(
             'local' => 'sitio_id',
             'foreign' => 'id'));

        $this->hasOne('Reglamentacion', array(
             'local' => 'reglamentacion_id',
             'foreign' => 'id'));
    }
}